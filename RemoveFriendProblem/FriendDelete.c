#include<stdio.h>
#include<stdlib.h>
 
struct friend{
    int popularity ;
    struct friend *next;
};
typedef struct friend friend;
 
friend* createNode(int  popularity ){
    friend* newFriend = (friend*) malloc(sizeof(friend));
    newFriend->popularity = popularity;
    newFriend->next = NULL;
    return newFriend;
}
 
friend* addFriendNode(friend* friendList, friend* newFriend){
   if(friendList){ 
       newFriend->next = friendList;
    }
    return newFriend;
}
 
friend* deleteFriendNode(friend* friendList){
    if(friendList == NULL){
      return friendList; 
    }
    friend *tempFriendList = friendList->next;
    free(friendList);
    return tempFriendList;
}
 
void printFriendList(friend* head){
    if(head == NULL)
        return;
    printFriendList(head->next);
    printf("%d ", head->popularity);
}

void checkAndRemoveFriend(numberOfFriends, deleteCounter){
    int count, popularity;
    friend* friendList = NULL, *newFriend;
    for(count=0; count<numberOfFriends; count++){
        scanf("%d", &popularity);
        newFriend = createNode(popularity);
        while(friendList!=NULL && friendList->popularity < popularity && deleteCounter){
            deleteCounter--;
            friendList = deleteFriendNode(friendList);
        }
        friendList = addFriendNode(friendList, newFriend);
    }
        printFriendList(friendList);
        printf("\n");
}
 
int main(){
    int testCases, numberOfFriends, friendstoDelete;
    scanf("%d", &testCases);
    while(testCases--){
        scanf("%d %d", &numberOfFriends, &friendstoDelete);
        checkAndRemoveFriend(numberOfFriends, friendstoDelete);
    }
    return 0;
}