package matrix;

import java.awt.image.SampleModel;

public interface IArrayAddition {
	int[][] convertInputToArray(String input);
	String addAlternateArrayElements(int sampleInput[][]);
}
