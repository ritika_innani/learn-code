package matrix;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArrayAdditionTest {

	@Test
	public void testConvertInputToArray_CheckForNull() {
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 8 9";
		
		int sampleInput[][] = arrayAddition.convertInputToArray(input);
		
		assertNotNull(sampleInput[0][0]);
		assertNotNull(sampleInput[1][1]);
		assertNotNull(sampleInput[2][2]);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testConvertInputToArray_CheckForNumericValue() {
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 a 6 7 8 9";
		
		arrayAddition.convertInputToArray(input);
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testConvertInputToArray_CheckIfOutOfRangeElementIsAccessed() {
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 8 9";
		
		int sampleInput[][] = arrayAddition.convertInputToArray(input);
		
		assertEquals(10, sampleInput[3][0]);
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testConvertInputToArray_CheckIfNoOfElementsAreLess() {
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7";
		
		arrayAddition.convertInputToArray(input);
	}
	
	@Test
	public void testConvertInputToArray_CheckForValue() {
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 8 9";
		
		int sampleInput[][] = arrayAddition.convertInputToArray(input);
		
		assertEquals(sampleInput[0][0], Integer.parseInt(input.split(" ")[0]));
		assertEquals(sampleInput[1][1], Integer.parseInt(input.split(" ")[4]));
		assertEquals(sampleInput[2][2], Integer.parseInt(input.split(" ")[8]));
	}
	
	@Test
	public void testAddAlternateArrayElements_shouldGiveCorrectAnswer() {
		ArrayAddition arrayAddition = new ArrayAddition();
		int sampleInput[][] = {{1,2,3},{4,5,6},{7,8,9}};
		
		String result = arrayAddition.addAlternateArrayElements(sampleInput);
		
		assertNotNull(result);
		assertEquals(result.split(" ")[0], "25");
		assertEquals(result.split(" ")[1], "20");
	}

}
