public class UseCustomHashMap{
	public static void main(String[] args) {
		CustomHashMap<String, String> customHashMap = new CustomHashMap<String,String>();
		customHashMap.put("kim", "football");
		customHashMap.put("matt", "cricket");
		customHashMap.put("chan", "badminton");
		customHashMap.put("jason", "cricket");
		System.out.println(customHashMap.get("matt"));
		System.out.println(customHashMap.get("jason"));
		customHashMap.print();
		customHashMap.remove("matt");
		customHashMap.print();
	}
}
