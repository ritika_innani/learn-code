import java.util.ArrayList;
import java.util.LinkedList;

class HashNode<K,V>{
	K key;
	V value;
	int hashCode;
	
	public HashNode(K key,V value) {
		this.key = key;
		this.value = value;
		hashCode = key.hashCode();
	}
}

public class CustomHashMap<K,V> {
	K key;
	V value;
	final int HASH_BUCKET_SIZE = 10;
	ArrayList<LinkedList<HashNode<K,V>>> hashBucket = new ArrayList<LinkedList<HashNode<K,V>>>();
	public CustomHashMap() {
		for(int index = 0; index < HASH_BUCKET_SIZE; index++){
			hashBucket.add(null);
		}
	}
	
	public void put(K key, V value){
		int currentHashCode = key.hashCode();
		int currentIndex = currentHashCode % 10;
		if(hashBucket.get(currentIndex) != null){
			LinkedList<HashNode<K,V>> currentLinkedList = hashBucket.get(currentIndex);
			currentLinkedList.add(new HashNode<K,V>(key,value));
		}
		else{
			LinkedList<HashNode<K,V>> temp = new LinkedList<HashNode<K,V>>();
			temp.add(new HashNode<K,V>(key, value));
			hashBucket.set(currentIndex,temp);
		}
	}
	public V get(K key){
		int currentHashCode = key.hashCode();
		int currentIndex = currentHashCode % 10;
		if(hashBucket.get(currentIndex) != null){
			LinkedList<HashNode<K,V>> temp = hashBucket.get(currentIndex);
			for(HashNode<K,V> hashnode: temp)
			{
				if(hashnode.key.equals(key))
				{
					return hashnode.value;
				}
			}
		}
		else{
			return null;
		}
		return null;
	}
	public void print(){
		for(LinkedList<HashNode<K,V>> currentList:hashBucket){
			if(currentList == null)
				continue;
			for(HashNode<K,V> currentNode:currentList){
				System.out.println("<"+currentNode.key+","+currentNode.value+">\n");
			}
		}
	}
	public void remove(K key){
		int hashCode = key.hashCode();
				int currentIndex = hashCode % 10;
				LinkedList<HashNode<K,V>> currentList = hashBucket.get(currentIndex);
				for(HashNode<K,V> currentHashNode:currentList){
					if(currentHashNode.key.equals(key)){
						currentList.remove(currentHashNode);
					}
				}
	}
}
