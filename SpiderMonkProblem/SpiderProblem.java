package com.itt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class SpiderMonk 
{
	int totalS_selectS[]=new int[2];
	int k=0;
	Queue<DataType> arrayList = new LinkedList<DataType>();
	
	public SpiderMonk() // initiliz all values
	{
		try 
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			for(String s1 : br.readLine().split(" "))
			totalS_selectS[k++] = Integer.parseInt(s1);
			int temp=1;
			for(String powers : br.readLine().split(" "))
			{	
				DataType dt = new DataType();
				dt.setIndex(temp++);
				dt.setPower(Integer.parseInt(powers));
				arrayList.add(dt);
			}
		} 
		 catch (IOException e) 
		 {
			e.printStackTrace();
		 }
	}
	
	public static void main(String[] args) 
	{
		SpiderMonk spidermonk = new SpiderMonk();
		for(int i=0;i<spidermonk.totalS_selectS[1];i++)
		{
		Queue<DataType> selectedpowers = spidermonk.selectFirstSome(spidermonk.arrayList);
		DataType max = spidermonk.findMax(selectedpowers);
		spidermonk.removeMaxPowerAndDecrease(max,selectedpowers);
		spidermonk.enque(selectedpowers);
		}
	}


	private void enque(Queue<DataType> selectedpowers) 
	{
		arrayList.addAll(selectedpowers);
	}

	private void removeMaxPowerAndDecrease(DataType max,Queue<DataType> selectedpowers) 
	{
		System.out.print(max.getIndex()+" ");
		selectedpowers.remove(max);
		for(DataType d: selectedpowers)
		{
			if(d.getPower()-1>=0)
			d.setPower(d.getPower()-1);
		}
		
	}

	private DataType findMax(Queue<DataType> selectedpowers) 
	{
		int max = -1000;
		DataType maxobj = null;
		int i=0;
		Iterator<DataType> it = selectedpowers.iterator();
		//for(int i=0;i<selectedpowers.size();i++)
		while(it.hasNext())
		{
			DataType dt = it.next();
			if(dt.getPower()>max)
			{
				maxobj = dt;
				max = dt.getPower();
			}
		}
	
		return maxobj;
	}

	private Queue<DataType> selectFirstSome(Queue<DataType> arrayList2)
	{
		Queue<DataType> firstSome = new LinkedList<DataType>();
			
		int availableSpiders = totalS_selectS[1];
		if(arrayList2.size() <= totalS_selectS[1])
		{
			availableSpiders = arrayList2.size();
		}
		for(int i=0;i<availableSpiders;i++)
			
			firstSome.add(arrayList2.poll());
		
		
		return firstSome;
	}
}
class DataType
{
	int power;
	int index;
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
}