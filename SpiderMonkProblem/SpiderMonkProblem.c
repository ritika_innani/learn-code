#include <stdio.h>
#include <string.h>
#include<math.h>
#include<stdlib.h>

int main()
{
    int totalSpiders, spidersToBeSelected, *powerOfSpiders, index1, index2, position = 0, maxPowerIndex, maxPower = -1, startPoint;
	
    scanf("%d%d", &totalSpiders, &spidersToBeSelected);     
	
    powerOfSpiders = (int*)calloc(totalSpiders, sizeof(int));
    
    for(index1 = 0; index1 < totalSpiders; index1++){
        scanf("%d", &powerOfSpiders[index1]);
    }
	
    for(index1 = 0; index1 < spidersToBeSelected; index1++){ 
        startPoint = position;
		maxPower = -1;
		if(startPoint >= totalSpiders)
			startPoint = startPoint - totalSpiders;
	
		for(index2 = spidersToBeSelected; index2 >= 1;)
		{
			if(position >= totalSpiders)  // if position variable reaches end of the array or more 
				position -= totalSpiders;
			if((startPoint == position) && (index2 != spidersToBeSelected))
				break;
			else
			{
				if(powerOfSpiders[position] != -9)
				{
					if(maxPower < powerOfSpiders[position])
					{
						maxPower = powerOfSpiders[position];
						maxPowerIndex = position;
					}
					if(powerOfSpiders[position] != 0)
						powerOfSpiders[position] = powerOfSpiders[position] - 1;
					position++;
					index2--;
				}
				else
					position++;
			}
		}
		powerOfSpiders[maxPowerIndex] =- 9;
		printf("%d ",maxPowerIndex+1);
    }
    
    return 0;
}
