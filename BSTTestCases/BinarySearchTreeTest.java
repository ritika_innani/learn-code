package newTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.tests.AllTestsTest.JUnit4Test;
public class BinarySearchTreeTest extends JUnit4Test{
	
	@Test
	public void testNewNodeCheckNotNull(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		assertNotNull(binarySearchTree.newNode(5));
	}
	
	@Test
	public void testNewNodeInserted(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		assertEquals(5,(binarySearchTree.newNode(5)).Value);
		assertNull((binarySearchTree.newNode(5)).leftChild);
		assertNull((binarySearchTree.newNode(5)).rightChild);
	}
	
	@Test
	public void testNewNodeInsertedOnLeftRight(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		BstNode rootNode = null;
		rootNode = binarySearchTree.insertnode(rootNode, 67);
		assertEquals(67, rootNode.Value);
		rootNode = binarySearchTree.insertnode(rootNode, 55);
		rootNode = binarySearchTree.insertnode(rootNode, 78);
		assertEquals(55,rootNode.leftChild.Value);
		assertEquals(78,rootNode.rightChild.Value);
	}
	
	@Test
	public void treeHeightWhenNull(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		BstNode rootNode = null;
		assertEquals(0, binarySearchTree.treeHeight(rootNode));
	}
	
	@Test
	public void treeHeightWithOnlyLeftChlid(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		BstNode rootNode = null;
		rootNode = binarySearchTree.insertnode(rootNode, 67);
		rootNode = binarySearchTree.insertnode(rootNode, 55);
		rootNode = binarySearchTree.insertnode(rootNode, 43);
		rootNode = binarySearchTree.insertnode(rootNode, 32);
		assertEquals(4, binarySearchTree.treeHeight(rootNode));
	}
	
	@Test
	public void treeHeightWithOnlyRightChild(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		BstNode rootNode = null;
		rootNode = binarySearchTree.insertnode(rootNode, 1);
		rootNode = binarySearchTree.insertnode(rootNode, 11);
		rootNode = binarySearchTree.insertnode(rootNode, 22);
		assertEquals(3, binarySearchTree.treeHeight(rootNode));
	}
	
	@Test
	public void treeHeightWithChildNode(){
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		BstNode rootNode = null;
		rootNode = binarySearchTree.insertnode(rootNode, 55);
		rootNode = binarySearchTree.insertnode(rootNode, 43);
		rootNode = binarySearchTree.insertnode(rootNode, 68);
		rootNode = binarySearchTree.insertnode(rootNode, 59);
		rootNode = binarySearchTree.insertnode(rootNode, 72);
		assertEquals(3, binarySearchTree.treeHeight(rootNode));
	}
	
}
