import java.util.*;
import java.io.*;
public class Statistics {
    
    public void readAndCheckInput(){
		Scanner scan=new Scanner(System.in);
		int totalEntries = scan.nextInt();
		HashSet<String> set=new HashSet<>();
		HashMap<String,Integer> map=new HashMap<>();
		
		String result="";
		int count=-1;
		
		for(int index=0; index<totalEntries; index++){
			String name=scan.next();
			String game=scan.next();
			if(!set.contains(name)){
				if(map.get(game)==null){
					map.put(game,1);
					if(count<1){
						count=1;
						result=game;
					}
				}
				else{
					//map.put(game,map.get(game)+1);
					int temp=map.get(game);
					if((temp+1)>count){
						count+=1;
						result=game;
					}
					map.put(game,temp+1);
				}
				set.add(name);
			}
		}
		printResult(result, map.get("football"));
    }
    
    public void printResult(String result, Integer football){
        PrintWriter out=new PrintWriter(System.out,false);
        out.println(result);
		if(football==null)
			out.println(0);
		else
			out.println(football);
		out.close();
    }
    
	public static void main(String args[]){
    	Statistics statistics = new Statistics();  
    	statistics.readAndCheckInput();
	}
}