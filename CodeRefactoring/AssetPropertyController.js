/*global angular*/
/*jslint nomen:true*/
(function () {
  'use strict';

  function AssetPropertyController($window, $routeParams, $rootScope, $scope, $log, $filter, Configuration, ViewEvent,
                                   AssetProperties, safeApply, DeviceDataSetService, HtmlFactory,
                                   AssetService, IconService, NotifierService, Utils, SharedService, CanvasAsset,
                                   AssetType, DeviceColorMap, StageHelper, AssetMapService, RequestBuilder,
                                   OutcomeService, TransitionHelper, HpMasterDataMapService, translateFilter,
                                   ProjectSetting, AccessoriesDataMapService, OutcomeType, DeviceAccessoriesService,
                                   $timeout, AssetPlacementHelper, AmpvCalculationService, LevelType,
                                   PricingHelperService, LevelService, ZoneService, LevelEntityMapService,
                                   AssetLookUpService, PageVolumeCalculationService, AppSettingService, PageVolumeService,
                                   AssetMapHandler) {
    var calculatedTotalLastReading,
        calculatedTotalMonthlyReading,
        isHasColorChanged = false,
        isManualAssetAssign = false,
        isReplaceWithUnknown = false,
        isAssetSelect = false,
        isProductNumberChange = false,
        isLocalCurrency = false,
        isLocal = true,
        isManualAndLocalCurrency = true,
        isManualPricing = true,
        buttonModals = {},
        onSelectEventData = '',
        ONE = 1,
        dateFields = [
          AssetProperties.DATE_INTRODUCED,
          AssetProperties.DATE_INSTALLED,
          AssetProperties.DATE_MANUFACTURED,
          AssetProperties.PRICE_DATE,
          AssetProperties.LEASE_END,
          AssetProperties.LEASE_START
        ],
        searchFields = [
          AssetProperties.ASSET_NUMBER,
          AssetProperties.SERIAL_NUMBER,
          AssetProperties.HOST_NAME,
          AssetProperties.IP_ADDRESS,
          AssetProperties.MAC_ADDRESS
        ],
        dispositionDataSetKeys = [
          AssetProperties.ACTUAL_MONTHLY_PAGE_VOLUME_MONO,
          AssetProperties.ACTUAL_MONTHLY_PAGE_VOLUME_COLOR,
          AssetProperties.DIFF_MONTHLY_PAGE_VOLUME_MONO,
          AssetProperties.DIFF_MONTHLY_PAGE_VOLUME_COLOR,
          AssetProperties.PERCENT_MONTHLY_PAGE_VOLUME_MONO,
          AssetProperties.PERCENT_MONTHLY_PAGE_VOLUME_COLOR,
          Configuration.replacementAsset
        ],
        CONFIDENCE = {
          HIGH: 'Confidence 1-High',
          MEDIUM: 'Confidence 2-Medium',
          LOW: 'Confidence 3-Low'
        },
        MPS_SUPPORTED = {
          YES: 'yes',
          NO: 'no',
          UNKNOWN: 'unknown'
        },
        BUTTON_MODEL = {
          MODEL_LOOKUP: 'modelLookup',
          APPLY_TO_ALL: 'applyToAll'
        },
        projectId = $routeParams.id,
        //asset to be assigned.
        tempAssignee = null,
        exchangeRates = [],
        labelSettingsMap = {
          mapId: 'mapID',
          assetNumber: 'assetNumber',
          makeAndModel: 'make',
          model: 'model',
          ipAddress: 'iPAddress',
          monthlyPageVolumeMonoColorTotal: 'monthlyPageVolumeMonoColorTotal',
          deviceType: 'deviceType'
        };
    //the following two scope objects holds the ng-change functions that will be triggered.
    //always use ng-change instead of adding watchers. That will avoid the
    //labyrinth code we end up having while using watchers.
    $scope.onInputChange = {
      monoTotalMonthly: function () {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, Configuration.TRANSFER.MONO);
        populatePricingTabData();
      },
      colorTotalMonthly: function () {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, Configuration.TRANSFER.COLOR);
        populatePricingTabData();
      },
      monoTotalLastReading: function () {
        $scope.dynamicModel['monoTotalLastReadingIsChange'] = true;
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        populatePricingTabData();
        delete $scope.dynamicModel.monoTotalLastReadingIsChange;
      },
      colorProMonthlyVolume: function () {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, Configuration.TRANSFER.COLOR_PRO);
        populatePricingTabData();
      },
      colorTotalLastReading: function () {
        $scope.dynamicModel['colorTotalLastReadingIsChange'] = true;
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        populatePricingTabData();
        delete $scope.dynamicModel.colorTotalLastReadingIsChange;
      },
      serviceID: function () {
        $scope.dynamicModel.dateInstalled = AmpvCalculationService.decodeServiceID($scope.dynamicModel.serviceID);
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
      }
    };
    $scope.onDateChange = {
      dateInstalled: function () {
        if (!AmpvCalculationService.isOfHigherOrder(Configuration.ampvCalculationMethod.fromDateInstalled)) {
          AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        }
      },
      dateManufactured: function () {
        if (!AmpvCalculationService.isOfHigherOrder(Configuration.ampvCalculationMethod.fromDateManufactured)) {
          AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        }
      }
    };
    //the following two scope objects holds the ng-change functions that will be triggered.
    //always use ng-change instead of adding watchers. That will avoid the
    //labyrinth code we end up having while using watchers.
    $scope.onInputChange = {
      monoTotalMonthly: function () {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, Configuration.TRANSFER.MONO);
        populatePricingTabData();
      },
      colorTotalMonthly: function () {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, Configuration.TRANSFER.COLOR);
        populatePricingTabData();
      },
      monoTotalLastReading: function () {
        $scope.dynamicModel['monoTotalLastReadingIsChange'] = true;
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        populatePricingTabData();
        delete $scope.dynamicModel.monoTotalLastReadingIsChange;
      },
      colorProMonthlyVolume: function () {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, Configuration.TRANSFER.COLOR_PRO);
        populatePricingTabData();
      },
      colorTotalLastReading: function () {
        $scope.dynamicModel['colorTotalLastReadingIsChange'] = true;
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        populatePricingTabData();
        delete $scope.dynamicModel.colorTotalLastReadingIsChange;
      },
      serviceID: function () {
        $scope.dynamicModel.dateInstalled = AmpvCalculationService.decodeServiceID($scope.dynamicModel.serviceID);
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
      }
    };
    $scope.onDateChange = {
      dateInstalled: function () {
        if (!AmpvCalculationService.isOfHigherOrder(Configuration.ampvCalculationMethod.fromDateInstalled)) {
          AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        }
      },
      dateManufactured: function () {
        if (!AmpvCalculationService.isOfHigherOrder(Configuration.ampvCalculationMethod.fromDateManufactured)) {
          AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        }
      }
      //TODO:This is commented because date introduced will always come from DDs and will overwrite user entered value.
      // dateIntroduced: function () {
      //   if (!AmpvCalculationService.isOfHigherOrder(Configuration.ampvCalculationMethod.fromDateIntroduced)) {
      //     AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
      //   }
      // }
    };

    /**
     * On Accessories Delete call back
     * @param $event
     */
    function onAccessoriesDelete($event) {
      if (!$rootScope.token.featureMatrix.accessory) {
        return;
      }
      // In case of asset reset we will not have asset so we dont need to populate accessories
      if ($scope.selectedAsset) {
        $scope.accessoriesData = DeviceAccessoriesService.populateAccessoriesByAsset($scope.selectedAsset);
      }
    }

    //===========================================================
    //                    Map Id Enumeration START
    //===========================================================

    /**
     * Function responsible to update a mapId in project setting for next enumeration.
     * @param request
     */
    function updateProjectMapId(request) {
      var updateMapIdRequest,
          mapId = parseInt(request.mapID);

      // changing 0 or any negative values in MapId field should reject the value.
      if (mapId <= 0) {
        //TODO: need to check whether we should show toaster message or simply rejecting the value.
        $log.info("You are enter wrong value for mapId, So rejecting it.");
        $scope.dynamicModel.mapID = '';
        delete request.mapID;
      } else if (request.mapID && !isNaN(request.mapID)) { // it should be number to reset the enumerated counter.
        updateMapIdRequest = {
          projectId: projectId,
          lastUpdatedMapId: mapId
        };
        // check for duplicate map id.
        if (SharedService.isDuplicateMapId(request.mapID) && SharedService.isCurrentState()) {
          NotifierService.warning('MAP_ID_ALREADY_EXIST');
        }
        // should update the lastMapId in backend project setting for next time enumerating.
        ProjectSetting.updateProjectSetting(updateMapIdRequest);
      }

      return request;
    }

    /**
     * Function is responsible to enumerate the map id for given asset and finally trigger the given function.
     * @param request {@Code Object}
     * @param isRequired {@Code Boolean}
     * @param callback {@Code Function}
     */
    function enumeratedMapId(request, isRequired, callback) {
      var isCurrentState = SharedService.isCurrentState();

      // error callback
      function onError(error) {
        $log.error('Error while updating MapId', error);
        // should trigger the callback function
        callback(request);
      }

      // success callback
      function onSuccess(response) {
        $scope.dynamicModel.mapID = response.mapID;
        request.mapID = response.mapID;

        // check duplicate when an asset get enumerated mapId.
        if (SharedService.isDuplicateMapId(response.mapID) && isCurrentState) {
          NotifierService.warning('MAP_ID_ALREADY_EXIST');
        }

        // should trigger the callback function
        callback(request);
      }

      // validating...
      if (isRequired) {
        // enumerating...
        AssetMapService.enumerateMapId(projectId).then(onSuccess, onError);
      } else if (SharedService.isDuplicateMapId(request.mapID) && isCurrentState) {
        NotifierService.warning('MAP_ID_ALREADY_EXIST');
        callback(request);
      } else {
        callback(request);
      }

    }

    //===========================================================
    //                    Map Id Enumeration END
    //===========================================================


    /**
     * It is responsible to disable the button and also add the class based on that.
     * It mainly check original make and model value.
     * @param dynamicModel
     * @param defaultValue
     */
    function disableMatchButton(dynamicModel, defaultValue) {
      var ENABLE_CLASS_NAME = 'modelLookupEnable';
      dynamicModel[BUTTON_MODEL.MODEL_LOOKUP].isDisabled = !(dynamicModel[AssetProperties.ORIGINAL_MAKE] ||
          dynamicModel[AssetProperties.ORIGINAL_MODEL]) || defaultValue;

      // for changing class name based on disabling or enabling of button.
      dynamicModel[BUTTON_MODEL.MODEL_LOOKUP].class = dynamicModel[BUTTON_MODEL.MODEL_LOOKUP].isDisabled ? '' : ENABLE_CLASS_NAME;
    }

    /**
     * This function is responsible to disable the button based on the given key.
     * @param key {String} Model key.
     * @param dynamicModel {Object}
     * @param isDisabled {Boolean} default value of this property.
     */
    function disableButtonByKey(key, dynamicModel, isDisabled) {

      // in case when buttons models not bind with dynamicModel.
      if (!dynamicModel.hasOwnProperty(key)) {
        angular.extend(dynamicModel, buttonModals);
      }

      switch (key) {
        case BUTTON_MODEL.MODEL_LOOKUP:
          disableMatchButton(dynamicModel, isDisabled);
          break;
        case BUTTON_MODEL.APPLY_TO_ALL:
          //TODO: if any requirement may come.
          break;
      }
    }

    /**
     * This handler take care for enabling and disabling of APW button's.
     * @param buttonKeys
     * @param dynamicModel
     */
    function initButtonDisableHandler(buttonKeys, dynamicModel) {
      angular.forEach(buttonKeys, function (value, key) {
        disableButtonByKey(key, dynamicModel, true);
      });
    }

    /**
     * Populate accessories data
     * This function will provide the accessories from data set or default accessories.
     * Note : It will not provide the accessories saved in database.
     */
    function populateAccessoriesData() {
      if (!$rootScope.token.featureMatrix.accessory) {
        return;
      }
      if ($scope.dynamicModel) {
        var populatedAsset, accessoriesData;
        populatedAsset = angular.copy($scope.selectedAsset);
        populatedAsset.productNumber = $scope.dynamicModel.productNumber;
        AccessoriesDataMapService.getAccessoriesMapByAsset(populatedAsset).then(function (response) {
          accessoriesData = response;
          if (Utils.isEmpty(accessoriesData)) {
            accessoriesData = DeviceAccessoriesService.assignDefaultAccessories();
          }
          $scope.accessoriesData = accessoriesData;
        });
      }
    }

    /**
     * Tells whether device data is updated or not. If asset's device data set is updated then we
     * need to remove it's emptiness or set it's 'isManual' property to false. So that key search
     * can disabled
     * @param changes
     * @param selectedAsset
     * @returns {Boolean|boolean|*}
     */
    function isDeviceDataSetUpdated(changes, selectedAsset) {
      var hasModel = Utils.toBoolean(changes.model),
          hasMake = Utils.toBoolean(changes.make);
      hasModel = hasModel || Utils.toBoolean(selectedAsset.model);
      hasMake = hasMake || Utils.toBoolean(selectedAsset.make);
      return hasModel && hasMake;
    }

    function removeAutoSearchItem(asset) {
      function removeItem(collection, item) {
        if (!item) {
          return;
        }

        collection.forEach(function (data, index) {
          if (data === item) {
            collection.splice(index, 1);
          }
        });
      }

      searchFields.forEach(function (field) {
        removeItem($scope.dataSet[field], asset[field]);
      });
    }

    /*
     * Check exclude design checkbox in changes and update dispositionIntentions
     * and Check dispositionIntentions in changes and update exclude design checkbox
     */
    function isAssetExcluded(changes) {
      var isIntentionOutOfScope = $scope.dynamicModel.dispositionIntentions === Configuration.dispositionIntentions.excludeDevice,
          isNotExcluded = !changes.excludeFromDesign && Utils.isUndefined(changes.dispositionIntentions) && isIntentionOutOfScope;
      if (changes.excludeFromDesign) {
        changes.dispositionIntentions = Configuration.dispositionIntentions.excludeDevice;
      } else if (isNotExcluded) {
        changes.dispositionIntentions = '';
      }
      if (isIntentionOutOfScope && Utils.isUndefined(changes.excludeFromDesign)) {
        changes.excludeFromDesign = true;
        changes.dispositionIntentions = Configuration.dispositionIntentions.excludeDevice;

      } else if (!isIntentionOutOfScope && !changes.excludeFromDesign && $scope.dynamicModel.dispositionIntentions) {
        changes.excludeFromDesign = false;
      }
    }

    /**
     * Call to be executed when user added ip address
     * validate ip address and update connection type
     * @param oldValue
     * @param newValue
     */
    function ipAddressWatcher(newValue, oldValue) {
      if ($scope.selectedAsset && ($scope.selectedAsset.iPAddress === newValue)) {
        // if connectionType is set manually or ip address is not updated then need not to change the connection type simple return.
        if ($scope.selectedAsset.connectionType) {
          return;
        }
        //If we are getting connectivity field in dynamic model then need not to change the connection type simple return.
        if ($scope.selectedAsset.connectivity) {
          $scope.dynamicModel.connectionType = $scope.selectedAsset.connectivity.toLowerCase();
          return;
        }
      }
      $scope.dynamicModel.connectionType = Utils.isValidIpAddress(newValue) ? Configuration.CONNECTION_TYPE.NETWORK : '';
    }

    /**
     * Adds the auto search entries of the asset to the auto complete data set.
     * It will be mainly used when will unmap the asset.
     * @param asset
     */
    function addAutoSearchItem(asset) {

      function addItem(collection, item) {
        if (!item) {
          return;
        }
        collection.push(item);
      }

      searchFields.forEach(function (field) {
        addItem($scope.dataSet[field], asset[field]);
      });
    }

    /**
     * This function will execute when Angular cycle about to destroy.Put those stuff inside this fn
     * which required to clean at the time of scope distorson.
     * */
    function onScopeDestroy() {
      //searchFields are the autosearch fields like assetNumber,serialNumber etc.
      AssetService.clearSet(searchFields);
    }

    /**
     * Check if assignee is already assigned to current selected asset.
     * @param asset asset to be assigned
     * @returns {boolean}
     */
    function isAlreadyAssigned(asset) {
      if (tempAssignee) {
        return tempAssignee.getId() === asset.getId();
      }
      return false;
    }

    /**
     * Tells whether the property is a date field or not. It will be mainly used for populating the
     * data to the window so that we can avoid ngModel date format error.
     * @param fieldKey
     * @returns {boolean}
     */
    function isDateField(fieldKey) {
      return dateFields.indexOf(fieldKey) !== -1;
    }

    function validateUpdateRequest(data) {
      if (Utils.isEmpty(data)) {
        $log.info('Nothing to update in asset properties.');
        return 'NOTHING_TO_UPDATE';
      }

      // User might leave device type empty. Let's make it
      // "unknown" if it's empty
      if (data.hasOwnProperty(AssetProperties.DEVICE_TYPE)) {
        data.deviceType = data.deviceType || AssetType.UNKNOWN;
      }
      return null;
    }

    function toDateField(dateString, field) {
      if (dateFields.indexOf(field) === -1) {
        return;
      }
      var date = new Date(dateString);
      return (date && date.isValid()) ? date : undefined;
    }

    /**
     * As names suggests it remove the properties which are populated from matching
     * disposition set data. We don't want to send disposition data properties of asset to backend.
     * So if disposition data properties are required to removed then {@code cleanUpDispositionDataSetProperties}
     * needs to be invoked.
     * @param data object containing disposition data set properties.
     * @returns {*}
     */
    function cleanUpDispositionDataSetProperties(data) {
      dispositionDataSetKeys.forEach(function (key) {
        delete data[key];
      });
      return data;
    }

    /**
     * As names suggests it remove the properties which are populated from matching
     * button model key. We don't want to send button model data properties of asset to backend.
     * So if button model data properties are required to removed then {@code cleanUpButtonModelData}
     * needs to be invoked.
     * @param data object containing button model data properties.
     * @returns {*}
     */
    function cleanUpButtonModelData(data) {
      angular.forEach(buttonModals, function (value, key) {
        delete data[key];
      });
      return data;
    }

    /**
     * This function mainly responsible to populate the valid value in APW.
     * @param populateValue
     * @param key
     */
    function populateValidDeviceDataSetValue(populateValue, key) {

      // in case of value is not defined then no need to deal with that.
      if (Utils.isUndefined(populateValue) || Utils.isNull(populateValue)) {
        return;
      }

      var date = toDateField(populateValue, key);
      if (date) {
        $scope.dynamicModel[key] = date;
        return;
      }

      if (key === AssetProperties.IS_MPS_SUPPORTED && Utils.isBoolean(populateValue)) {
        $scope.dynamicModel[key] = populateValue === true ? MPS_SUPPORTED.YES :
            (populateValue === false ? MPS_SUPPORTED.NO : MPS_SUPPORTED.UNKNOWN);
        return;
      }

      $scope.dynamicModel[key] = populateValue;
    }

    // populating properties with given asset data preference.
    function populateAssetDataPreference(asset, dataSet, keys) {

      // populate callback
      function onPopulate(key) {
        // it override the asset id with product id and also for manual asset it keeps the previous asset.
        // So not allowing to populate these value here.
        if (key === AssetProperties.ID || key === AssetProperties.MAP_ID) {
          return;
        }

        var populateValue = !Utils.isUndefined(asset[key]) ? asset[key] : dataSet[key];
        populateValidDeviceDataSetValue(populateValue, key);
      }

      // populating...
      angular.forEach(keys, onPopulate);
    }

    // populating properties with given the device data set preference.
    function populateDeviceDataSetPreference(asset, dataSet, keys) {

      // populate callback
      function onPopulate(key) {
        // it override the asset id with product id and also for manual asset it keeps the previous asset.
        // So not allowing to populate these value here.
        if (key === AssetProperties.ID || key === AssetProperties.MAP_ID) {
          return;
        }

        var populateValue = null;
        if (!Utils.isUndefined(dataSet[key])) {
          if (key === AssetProperties.DEVICE_TYPE) {
            populateValue = dataSet[key] !== AssetType.UNKNOWN ? dataSet[key] : asset[key];
          } else {
            populateValue = dataSet[key];
          }
        } else {
          populateValue = asset[key];
        }

        populateValidDeviceDataSetValue(populateValue, key);
      }

      // populating...
      angular.forEach(keys, onPopulate);
    }

    /**
     * It is responsible to bind the asset data with HP Master Data based on supplied make & model.
     * @param asset {Asset} selected asset.
     */
    function bindAssetDataWithHpMasterData(asset) {

      //TODO: we have to change the below request key parameter make to originalMake and model to originalModel,
      // Should take care after @Task : CAR-6065 has been done.
      var request = {
            _id: asset.getId(),
            make: $scope.dynamicModel.originalMake.toString(),
            model: $scope.dynamicModel.originalModel.toString()
          },
          dataSet = angular.copy(DeviceDataSetService.getDeviceDataSet(request.make, request.model) || Configuration.DEFAULT_DATA_SET_MODEL);

      // Will show warning when searched asset is in wrong country
      if (SharedService.isTransitionState() && !SharedService.isSkuPresent(dataSet)) {
        NotifierService.warning('THIS_DEVICE_IS_UNSUPPORTED_IN_CURRENT_COUNTRY');
      }

      // return true if device is MPS.
      function isMpsDevice(data) {
        return data[AssetProperties.IS_MPS_SUPPORTED] === true;
      }

      // responsible for setting confidence.
      function shouldSetConfidence(data, defaultConfidence) {
        // should set confidence.
        if (!data.hasOwnProperty(AssetProperties.CONFIDENCE)) {
          data[AssetProperties.CONFIDENCE] = Utils.isUndefined(dataSet) ? defaultConfidence : CONFIDENCE.HIGH;
        }
      }

      // populate String Mapping Data.
      function populateStringMappingData(response) {
        // in case of low confidence.
        if (!response[AssetProperties.CONFIDENCE] || response[AssetProperties.CONFIDENCE] === CONFIDENCE.LOW) {
          $scope.dynamicModel[AssetProperties.MAKE] = '';
          $scope.dynamicModel[AssetProperties.MODEL] = '';
          delete response[AssetProperties.MAKE];
          delete response[AssetProperties.MODEL];
          NotifierService.info('NO_MATCH_FOUND');
        }

      }

      // success callback
      function onSuccess(response) {
        var uniqueKeys;
        dataSet = angular.copy(DeviceDataSetService.getDeviceDataSet(response.make, response.model) || Configuration.DEFAULT_DATA_SET_MODEL);

        // no need to populate the response when requested asset id and selected asset id is different.
        if (asset.getId() !== $scope.selectedAsset.getId()) {
          $log.warn("string mapping requested been for different asset.");
          return;
        }

        // in case of empty response we should populate the device data set.
        if (!response) {
          $log.warn("getting empty response.");
          shouldSetConfidence(dataSet, CONFIDENCE.LOW);
          populateAssetDataPreference(asset, dataSet, Object.keys(dataSet));
          return;
        }

        // based on response, based on that taking the appropriate action to populating the properties.
        if (isMpsDevice(response)) {
          // should set the confidence.
          shouldSetConfidence(response, CONFIDENCE.HIGH);
          $log.info('MPS device found, so populating device data set.');
        } else if (response.productId) {
          // should set the confidence.
          shouldSetConfidence(response, CONFIDENCE.HIGH);
          $log.info('Hp master data updated with asset properties.');
        } else {
          populateStringMappingData(response);
          $log.info('product id not found from string mapping.');
        }

        uniqueKeys = Utils.getUniqueArrayKeys(dataSet, response);
        populateDeviceDataSetPreference(response, dataSet, uniqueKeys);
        populateAccessoriesData();
      }

      // error callback
      function onError(error) {
        if (error.status === -1) {
          // when user went offline.
          $log.warn('Not able to get the HP Master Data in offline mode.');
          return;
        }
        // should populate asset data combine with device data set properties.
        shouldSetConfidence(dataSet, CONFIDENCE.LOW);
        populateAssetDataPreference(asset, dataSet, Object.keys(dataSet));
        populateAccessoriesData();
        NotifierService.error('ERROR_WHILE_RETRIEVING_HP_MASTER_DATA');
        $log.error('getting error while calling Euro Form API... ', error);
      }

      if ($rootScope.token.featureMatrix.hpMasterData) {
        // determine the following use cases, based on that taking the appropriate action.
        if (isMpsDevice(dataSet)) {
          shouldSetConfidence(dataSet, CONFIDENCE.HIGH);
          populateDeviceDataSetPreference(asset, dataSet, Object.keys(dataSet));
          populateAccessoriesData();
        } else if (dataSet[AssetProperties.PRODUCT_ID]) {
          HpMasterDataMapService.getHpMasterDataByProductId(dataSet[AssetProperties.PRODUCT_ID], asset.getId()).then(onSuccess, onError);
        } else {
          // It will call both string mapping and HP Master Data api.
          HpMasterDataMapService.getHpMasterData([request], asset.getId()).then(onSuccess, onError);
        }
      } else {
        AssetLookUpService.getStringMappingAPIData([request], asset.getId()).then(onSuccess, onError);
      }
      // disabling the match button once user click on it.
      if ($scope.dynamicModel.hasOwnProperty(BUTTON_MODEL.MODEL_LOOKUP)) {
        $scope.dynamicModel[BUTTON_MODEL.MODEL_LOOKUP].isDisabled = true;
        $scope.dynamicModel[BUTTON_MODEL.MODEL_LOOKUP].class = '';
      }

      // here it prevent to make model empty which getting empty by make value watcher.
      isAssetSelect = true;
      isProductNumberChange = false;
    }

    /**
     * Returns reading tab specific keys
     */
    function getReadingTabProperties() {
      return [
        AssetProperties.MONO_TOTAL_MONTHLY,
        AssetProperties.MONO_A4_MONTHLY,
        AssetProperties.MONO_A3_MONTHLY,
        AssetProperties.COLOR_TOTAL_MONTHLY,
        AssetProperties.COLOR_A4_MONTHLY,
        AssetProperties.COLOR_A3_MONTHLY,
        AssetProperties.COLOR_PRO_MONTHLY_VOLUME,
        AssetProperties.COLOR_PRO_LAST_READING,
        AssetProperties.LIFE_TOTAL_MONTHLY,
        AssetProperties.MONO_TOTAL_LAST_READING,
        AssetProperties.MONO_A3_LAST_READING,
        AssetProperties.MONO_A4_LAST_READING,
        AssetProperties.LIFE_A3_TOTAL_LAST_READING,
        AssetProperties.LIFE_A3_TOTAL_MONTHLY,
        AssetProperties.LIFE_A4_TOTAL_LAST_READING,
        AssetProperties.LIFE_A4_TOTAL_MONTHLY,
        AssetProperties.LIFE_TOTAL_LAST_READING,
        AssetProperties.COLOR_A4_LAST_READING,
        AssetProperties.COLOR_A3_LAST_READING,
        AssetProperties.COLOR_TOTAL_LAST_READING,
        AssetProperties.ACTUAL_MONTHLY_PAGE_VOLUME_MONO,
        AssetProperties.ACTUAL_MONTHLY_PAGE_VOLUME_COLOR,
        AssetProperties.ACTUAL_MONTHLY_PAGE_VOLUME_COLOR_PRO,
        AssetProperties.PROPOSED_MONTHLY_PAGE_VOLUME_MONO,
        AssetProperties.PROPOSED_MONTHLY_PAGE_VOLUME_COLOR,
        AssetProperties.PROPOSED_MONTHLY_PAGE_VOLUME_COLOR_PRO,
        AssetProperties.PROPOSED_MONO_MONTHLY_VOLUME,
        AssetProperties.TOTAL_PROPOSED_MONTHLY_VOLUME
      ];
    }

    /**
     * Function is responsible to populate the device data set properties
     * @param dataSet data to be populated
     */
    function onDeviceDataSetSuccess(dataSet) {
      var asset, productId;
      // if asset is going to be assigned then values should be picked from temp assignee
      // else $scope.selectedAsset will also do the magic.
      asset = tempAssignee || $scope.selectedAsset;

      // if asset is not defined then no need to proceed further.
      if (!asset) {
        return;
      }
      if (!Utils.isUndefined(dataSet)) {
        dataSet[AssetProperties.CONFIDENCE] = CONFIDENCE.HIGH;
      } else {
        dataSet = {};
        dataSet[AssetProperties.CONFIDENCE] = CONFIDENCE.LOW;
      }

      // Will show warning when searched asset is in wrong country
      if (SharedService.isTransitionState() && !SharedService.isSkuPresent(dataSet)) {
        NotifierService.warning('THIS_DEVICE_IS_UNSUPPORTED_IN_CURRENT_COUNTRY');
      }

      productId = dataSet[AssetProperties.PRODUCT_ID];

      // if user removed the make or model then reset the previous populated data.
      if (!($scope.dynamicModel.make && $scope.dynamicModel.model)) {
        populateDeviceDataSetPreference(asset, dataSet, Object.keys(dataSet));
        return;
      }

      // function is responsible to do the stuff which is compulsory in both success/failure cases and
      // also if product id is not there.
      function populate(data, keys) {
        populateDeviceDataSetPreference(data, dataSet, keys);
      }

      // success callback
      function onSuccess(response) {
        // populating the response when requested asset id and response id is same.
        if ($scope.selectedAsset && response._id === $scope.selectedAsset._id) {
          // setting the preference to DeviceDataSet over HP Master Data.
          var hpMasterData = angular.copy(response);
          angular.extend(hpMasterData, dataSet);
          if (dataSet[AssetProperties.DEVICE_TYPE] === AssetType.UNKNOWN && response.hasOwnProperty(AssetProperties.DEVICE_TYPE)) {
            hpMasterData[AssetProperties.DEVICE_TYPE] = response[AssetProperties.DEVICE_TYPE];
          }
          populate(hpMasterData, Object.keys(hpMasterData));
          $log.info("Updated with HP Master data.");
        }
      }

      // error callback
      function onError(error) {
        populate(asset, Object.keys(dataSet));
      }

      if ($rootScope.token.featureMatrix.hpMasterData && productId) {
        HpMasterDataMapService.getHpMasterDataByProductId(dataSet[AssetProperties.PRODUCT_ID], asset._id).then(onSuccess, onError);
      } else {
        populate(asset, Object.keys(dataSet));
      }
    }

    /**
     *  Function is responsible to populate the device data set properties along with HP Master data with setting
     * the preference to device data set.
     * It trigger when user changed make/model of an selected asset from APW.
     */
    function populateDeviceDataSetPropertiesWhenDataChanged(isProductNumber) {
      if (isProductNumber) {
        DeviceDataSetService.getDataSetFromProductNumber($scope.dynamicModel.productNumber, DeviceDataSetService.isDeviceDataSetLoaded())
            .then(onDeviceDataSetSuccess, function (error) {
              $log.error("error getting device data set", error);
            });
      } else {
        DeviceDataSetService.getDataSetFromMakeModel($scope.dynamicModel.make, $scope.dynamicModel.model, DeviceDataSetService.isDeviceDataSetLoaded())
            .then(onDeviceDataSetSuccess, function (error) {
              $log.error("error getting device data set", error);
            });
      }
    }

    /**
     * Function is responsible to populate the device data set properties along with HP Master data with setting
     * the preference to device data set.
     * It trigger when user select an asset.
     * @param excludeAMPVCalculation will contain info whether to calculate AMPV Calculation or not
     */
    function populateDeviceDataSetPropertiesWhenAssetSelect(excludeAMPVCalculation) {
      var asset, dataSet, productId;

      // if asset is going to be assigned then values should be picked from temp assignee
      // else $scope.selectedAsset will also do the magic.
      asset = tempAssignee || $scope.selectedAsset;

      // if asset or make/model is not defined or empty then no need to proceed further.
      if (!asset || !($scope.dynamicModel.make && $scope.dynamicModel.model)) {
        // in case when there is no make/model then mps may be blank, so init this also.
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel);
        if (!$scope.dynamicModel.hasOwnProperty(AssetProperties.IS_MPS_SUPPORTED)) {
          $scope.dynamicModel[AssetProperties.IS_MPS_SUPPORTED] = '';
        }
        return;
      }

      dataSet = DeviceDataSetService.getDeviceDataSet($scope.dynamicModel.make, $scope.dynamicModel.model) || Configuration.DEFAULT_DATA_SET_MODEL;
      productId = dataSet[AssetProperties.PRODUCT_ID];
      //this is used to check if the value is being assigned. in that case it should not calculate, since it should
      //copy the volumes correctly. it will be called on save.
      if (!excludeAMPVCalculation) {
        AmpvCalculationService.calculateAMPVandPopulate($scope.selectedAsset, $scope.dynamicModel, null, dataSet);
      }

      // function is responsible to do the stuff which is compulsory in both success/failure cases and
      // also if product id is not there.
      function populate(data, keys) {
        populateAssetDataPreference(asset, data, keys);
      }

      // success callback
      function onSuccess(response) {
        var requestedAssetId = tempAssignee ? tempAssignee._id : ($scope.selectedAsset ? $scope.selectedAsset._id : null);

        // populating the response when requested asset id and response id is same.
        if (response && response._id === requestedAssetId) {
          // setting the preference to DeviceDataSet over HP Master Data.
          angular.extend(response, dataSet);
          populate(response, Object.keys(response));
          $log.info("Updated with HP Master data.");
        }
      }

      // error callback
      function onError(error) {
        populate(dataSet, Object.keys(dataSet));
      }

      if ($rootScope.token.featureMatrix.hpMasterData && productId) {
        HpMasterDataMapService.getHpMasterDataByProductId(dataSet[AssetProperties.PRODUCT_ID], asset._id).then(onSuccess, onError);
      } else {
        populate(dataSet, Object.keys(dataSet));
      }
    }

    // TODO: we keep this code for future reference, will remove after getting verified.
    /**
     * As names suggests it remove the properties which are populated from matching
     * device data. We don't want to send device data properties of asset to backend.
     * So if device data properties are required to removed then {@code cleanUpDeviceDataSetProperties}
     * needs to be invoked.
     * @param data object containing device data set properties.
     * @returns {*}
     */

    /* function cleanUpDeviceDataSetProperties(data) {
     var make = data.make,
     model = data.model,
     deviceType = data.deviceType,
     dataSet = DeviceDataSetService.getDeviceDataSet(make, model);
     if (!dataSet) {
     // need not to do anything cause data set is not present.
     return data;
     }
     /!**
     * Closure to clean data. It dirty checks the data. It data is pristine the
     * it removes it.
     * @param value populates value
     * @param key name of the property.
     *!/
     function cleanCb(value, key) {
     var flagSame,
     date = toDateField(dataSet[key], key);
     if (date) {
     flagSame = $scope.dynamicModel[key].isSame(date);
     } else {
     flagSame = $scope.dynamicModel[key] === dataSet[key];
     }

     //TODO Remove this check(key !== AssetProperties.HAS_COLOR) After discussion

     if (flagSame && key !== AssetProperties.HAS_COLOR) {
     //no change is made is made, we need to delete it.
     delete data[key];
     }

     }

     angular.forEach(data, cleanCb);
     // properties like make and model might have remove so adding them.
     angular.extend(data, {
     make: make,
     model: model,
     deviceType: deviceType
     });

     return data;
     }
     */


    /**
     * Populates the model based on the make.
     * @param make
     */
    function populateModel(make) {
      make = make || '';
      var models = DeviceDataSetService.getModels(make);
      // Clear existing model and push new one
      $scope.dataSet.model.splice(0, $scope.dataSet.model.length);
      Array.prototype.push.apply($scope.dataSet.model, models);
    }

    /**
     * As name suggest it populates the last reading  properties for the asset.
     * If Life Total values not coming from DCA then it calculate
     * Life Total = mono total + color total
     * */
    function populateTotalLastReading(asset) {
      var hasLifeTotalLastReading, isLastReadinginValid;

      hasLifeTotalLastReading = $scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING];
      isLastReadinginValid = !Utils.isValid($scope.dynamicModel[AssetProperties.MONO_TOTAL_LAST_READING]) && !Utils.isValid($scope.dynamicModel[AssetProperties.COLOR_TOTAL_READING]);

      $scope.dynamicModel[AssetProperties.MONO_TOTAL_LAST_READING] =
          Utils.truncateDecimals($scope.dynamicModel[AssetProperties.MONO_TOTAL_LAST_READING]);

      $scope.dynamicModel[AssetProperties.COLOR_TOTAL_READING] =
          Utils.truncateDecimals($scope.dynamicModel[AssetProperties.COLOR_TOTAL_READING]);


      calculatedTotalLastReading = $scope.dynamicModel[AssetProperties.MONO_TOTAL_LAST_READING] +
          $scope.dynamicModel[AssetProperties.COLOR_TOTAL_READING];

      if (!hasLifeTotalLastReading && (!asset.hasTotalLastReading || asset.isLastReadingManualChanged)) {
        $scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING] = calculatedTotalLastReading;
        $scope.flagLifeTotal = true;
      } else {
        if (isNaN($scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING])) {
          // in case of invalid value for e.g. "#VALUE";
          $scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING] = '';
        }
        else {
          $scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING] =
              Utils.truncateDecimals($scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING]);
        }
      }

      if (isLastReadinginValid && (!$scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING] && $scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING] !== 0)) {
        $scope.dynamicModel[AssetProperties.LIFE_TOTAL_LAST_READING] = '';
      }
    }

    /**
     * As name suggest it populates the populateMonthlyReading properties for the asset.
     * If Monthly Total values not coming from DCA then it calculate
     * Monthly Total = mono total + color total
     * */
    function populateTotalMonthlyReading(asset) {
      var monoTotalMonthly, colorTotalMonthly, hasTotalMonthlyReading, colorProMonthly,
          isMonthlyTotalinValid;
      hasTotalMonthlyReading = $scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY];
      monoTotalMonthly = $scope.dynamicModel[AssetProperties.MONO_TOTAL_MONTHLY];
      colorTotalMonthly = $scope.dynamicModel[AssetProperties.COLOR_TOTAL_MONTHLY];
      colorProMonthly = $scope.dynamicModel[AssetProperties.COLOR_PRO_MONTHLY_VOLUME];
      isMonthlyTotalinValid = !Utils.isValid($scope.dynamicModel[AssetProperties.MONO_TOTAL_MONTHLY]) && !Utils.isValid($scope.dynamicModel[AssetProperties.COLOR_TOTAL_MONTHLY]);

      $scope.dynamicModel[AssetProperties.MONO_TOTAL_MONTHLY] = Utils.isValid(monoTotalMonthly) ? Utils.truncateDecimalValue(monoTotalMonthly) : '';
      $scope.dynamicModel[AssetProperties.COLOR_TOTAL_MONTHLY] = Utils.isValid(colorTotalMonthly) ? Utils.truncateDecimalValue(colorTotalMonthly) : '';
      $scope.dynamicModel[AssetProperties.COLOR_PRO_MONTHLY_VOLUME] = Utils.isValid(colorProMonthly) ? Utils.truncateDecimalValue(colorProMonthly) : '';

      calculatedTotalMonthlyReading = Utils.toNumber($scope.dynamicModel[AssetProperties.MONO_TOTAL_MONTHLY]) +
          Utils.toNumber($scope.dynamicModel[AssetProperties.COLOR_TOTAL_MONTHLY]) +
          Utils.toNumber($scope.dynamicModel[AssetProperties.COLOR_PRO_MONTHLY_VOLUME]);

      if ((!hasTotalMonthlyReading && (!asset.hasTotalMonthly || asset.isMonthlyReadingManualChanged)) ||
          (calculatedTotalMonthlyReading && (asset.lifeTotalMonthly !== calculatedTotalMonthlyReading))) {
        $scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY] = calculatedTotalMonthlyReading;
        $scope.flagLifeTotal = true;
      }

      else {
        if (isNaN($scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY])) {
          // in case of invalid value for e.g. "#VALUE";
          $scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY] = 0;
        }
        else {
          $scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY] =
              Utils.truncateDecimalValue($scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY]);
        }
      }
      if (isMonthlyTotalinValid && !$scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY]) {
        $scope.dynamicModel[AssetProperties.LIFE_TOTAL_MONTHLY] = '';
      }

    }

    /**
     * As name suggest it populates the Life Total properties for the asset.
     * If Life Total values not coming from DCA then it calculate
     * Life Total = mono total + color total
     * */
    function populateLifeTotal(asset) {
      // Populate last reading values only
      populateTotalLastReading(asset);
      // Populate Monthly reading values only
      //TODO: these values are being assigned and reassigned 3 times need to refactor this.
      populateTotalMonthlyReading(asset);
    }

    /**
     * As name suggest it populates the pricing tab properties for the asset.
     * */

    function populatePricingTabData(key, newValue, oldValue, param4) {
      if (oldValue && newValue && oldValue === newValue) {
        return;
      }
      try {
        var asset = $scope.selectedAsset, projectSettingData;
        projectSettingData = ProjectSetting.getProjectSettings($routeParams.id);
        if (!projectSettingData) {
          return;
        }
        projectSettingData.pricingSettings = projectSettingData.pricingSettings ? projectSettingData.pricingSettings : Configuration.defaultPricingSettings;
        $scope.pricingSource = projectSettingData.pricingSettings.pricingSource;
        isManualPricing = $scope.pricingSource === Configuration.defaultPricingSettings.pricingSource;
        isLocalCurrency = projectSettingData.pricingSettings.currency === Configuration.currencyOptions.LC;
        var countryDetails = getCurrentCountry(isLocalCurrency),
            //If countryName is valid then assign that value to current country else assign default currency - USD
            currentCountry = countryDetails || Configuration.defaultCountry.isoCurrencyCode;
        if (exchangeRates && exchangeRates.length) {
          PricingHelperService.getCurrencyRate(exchangeRates, isLocalCurrency, currentCountry.currency);
          $scope.selectedCurrencyCode = SharedService.selectedCurrencyCode();
        }
        var isDartPricingAllowed = $rootScope.token.featureMatrix.roleBaseAccess ?
            JSON.parse($rootScope.rolePermission.dartPricing || Configuration.TRUE) :
            !SharedService.isNonHpUser();
        $scope.flagNegativeCppMonthlySavings = PricingHelperService.populatePricingData(asset, $scope.pricingSource,
            $scope.dynamicModel, isLocalCurrency, isDartPricingAllowed);
      } catch (ex) {
        $log.error(ex);
      }
    }

    /**
     * Checks if the asset valid to be checked for current country
     * @param asset
     * @returns {boolean}
     */
    function isValidForSupportedCountry(asset) {
      return asset.getState() === AssetProperties.VIRTUAL_STATE || (asset.outcome === OutcomeType.REPLACE_WITH_EXISTING);
    }

    /**
     * Adds warning if asset is not present in current country
     * @param asset
     * @returns {string}
     */
    function getSupportedCountryWarning(asset) {
      if (!(asset.productNumber && isValidForSupportedCountry(asset))) {
        return '';
      }
      var dataSet = DeviceDataSetService.getDeviceDataSetFromProductNumber(asset.productNumber);
      if (dataSet && !AppSettingService.isSkuPresent(dataSet)) {
        return translateFilter('UNSUPPORTED_IN_COUNTRY');
      }
      return '';
    }

    /**
     * Set the zone names in MLT zones column
     * @param asset
     */
    function appendZoneName(asset) {
      $scope.dynamicModel.zone = ZoneService.getZoneName(asset._id);
    }

    /**
     * Populates the asset properties to model bound to asset property window
     * i.e. {@code $scope.dynamicModel}.
     * @param asset asset whose properties needs to be populated.
     * @param excludeAMPVCalculation will contain info whether to calculate AMPV Calculation or not
     */
    function populateAssetProperties(asset, excludeAMPVCalculation) {
      // in case of asset is null when we delete the floor, other than it's working expected.
      if (!asset) {
        return;
      }

      asset = angular.copy(asset);

      // If the selected asset is not present in the country than add warning
      asset.warning = getSupportedCountryWarning(asset);

      var fields, assetScheme = AppSettingService.getAssetSchemeSet(asset.getType());
      // get the required properties which needs to be populated,
      fields = SharedService.shouldPopulate(assetScheme.all);
      $scope.isDcaReadingDate = asset.isDcaReadingDate;

      function populateData(property) {

        // retaining the map id for replace with existing asset.
        if (property === AssetProperties.MAP_ID && isReplaceWithUnknown && $scope.selectedAsset) {
          $scope.dynamicModel[property] = $scope.selectedAsset.mapID;
          return;
        }
        //assign connection type on selected asset if not already assigned based on ipAddress.
        if (property === AssetProperties.CONNECTION_TYPE && asset[property] === undefined) {
          //If dca has connectivity field value -assign it to connection type
          if (asset.connectivity) {
            $scope.dynamicModel[property] = asset.connectivity.toLowerCase();
            return;
          }
          $scope.dynamicModel[property] = Utils.isValidIpAddress($scope.dynamicModel.iPAddress) ? Configuration.CONNECTION_TYPE.NETWORK : '';
          return;
        }
        var date;
        if (asset[property] !== undefined) {
          // angularjs ng-model for date field takes only date object so we need
          // to convert the value to date if date string is a valid date.
          if (isDateField(property) && asset[property]) { // date field should not be empty.
            date = new Date(asset[property]);
            if (date.isValid()) {
              $scope.dynamicModel[property] = new Date(date);
            } else {
              $log.warn('Invalid date string for "' + property + '": ' + asset[property]);
            }
          } else {
            $scope.dynamicModel[property] = asset[property];
          }
        }
      }

      //clearing previous value from model;
      $scope.dynamicModel = {};
      fields.forEach(populateData);
      appendZoneName(asset);
      // remove the previous page volume and assign the new one for updating when changes in proposed value.
      PageVolumeCalculationService.populatePageVolume($scope.dynamicModel, asset);
      $scope.dynamicModel.ampvCalculationMethod = asset.ampvCalculationMethod;
      $scope.dynamicModel.ampvCalculationMethodDescription = translateFilter(Configuration.ampvCalculationMethodDescription[$scope.dynamicModel.ampvCalculationMethod]);
      $scope.dynamicModel.replacementAsset = AssetPlacementHelper.getReplacementAssetLocation(asset.getId());
      if (!$scope.dynamicModel.lastReadingDate) {
        $scope.dynamicModel.lastReadingDate = '(' + Configuration.DEFAULT_DATE_FORMAT + ')';
        $scope.isDcaReadingDate = true;
      }
      populateDeviceDataSetPropertiesWhenAssetSelect(excludeAMPVCalculation);

      //Below condition is required b'z when we plot an asset manually then we need not to call populateModel function
      //If we call this Fn for manual plot then it would reset the dataSet.model blank for same model selection.
      if (!asset.isManual) {
        //if make found then populate model of make
        populateModel($scope.dynamicModel.make);
      }

      //disabled the recommendation (gray out - light-bulb) button if it already has the recommendation.
      $scope.isRecommendedAsset = asset.isRecommendedAsset || false;

      $scope.flagLifeTotal = PageVolumeCalculationService.populatePageVolume($scope.dynamicModel, asset);
      populatePricingTabData();
      populateLifeTotal(asset);

      //TODO: In near future we have to remove HTML Factory. So, sometime it not initialize below object, so using $timeout with (0) delay.
      // match button disable handler
      $timeout(function () {
        initButtonDisableHandler(buttonModals, $scope.dynamicModel);
      });
    }

    /**
     * Populates the asset properties based on a key search property and it's value.
     * This function is mainly responsible for key search.
     * @param asset
     * @param property
     */
    function populateAssetPropertiesBy(asset, property) {

      if (isAlreadyAssigned(asset)) {
        return;
      }

      tempAssignee = asset;
      //clear dynamic model of current asset;
      Object.keys($scope.dynamicModel).forEach(function (key) {
        //skipping id cause that need not be cleared.
        if (key === AssetProperties.ID) {
          return;
        }
        $scope.dynamicModel[key] = '';
      });
      populateAssetProperties(asset, property);
    }

    /**
     * Populate the selected asset properties.
     * @param asset {@code Asset}
     * @param excludeAMPVCalculation will contain info whether to calculate AMPV Calculation or not
     */
    function populateSelectedAsset(asset, excludeAMPVCalculation) {
      if (asset.hasTotal) {
        $scope.flagLifeTotal = true;
      }
      $scope.selectedAsset = asset;
      isReplaceWithUnknown = TransitionHelper.isReplaceWithUnknown($scope.selectedAsset.getOutcome());
      populateAssetProperties(asset, excludeAMPVCalculation);
    }

    /**
     * Populate Asset tabs Data
     * @param asset for which we need to populate tab data
     */
    function populateAssetTabData(asset) {
      if (!asset) {
        return;
      }


      safeApply($scope, function () {
        tempAssignee = null;
        isAssetSelect = true;
        isProductNumberChange = false;
        // Here we are updating asset data with euro form data.

        $scope.assetView = HtmlFactory.getAssetView(asset.getType());
        populateSelectedAsset(asset);
        if ($rootScope.token.featureMatrix.accessory) {
          $scope.accessoriesData = angular.copy(DeviceAccessoriesService.populateAccessoriesByAsset(asset));
        }
        $scope.setActiveTab(0); //make first tab i.e. basic tab active
      });
      //To apply pricing settings to all assets
      populatePricingTabData();
      if ($scope.pricingSource === Configuration.defaultPricingSettings.pricingSource && isLocal) {
        PricingHelperService.populateCostPerPage($scope.dynamicModel);
        PricingHelperService.populateBasePricingData($scope.dynamicModel);
      }
    }


    /**
     * Callback to be executed when asset select event is triggered.
     * @param $event broad casted asset select event.
     * @param asset selected asset. It can be from grid or canvas
     */
    function onAssetSelect($event, asset) {
      if (!asset) {
        return;
      }
      asset = CanvasAsset.prototype.isCanvasAsset(asset) ? AssetService.getAsset(asset.getId()) : asset;
      $scope.selectedAsset = asset;
      var configMatrix = SharedService.getConfiguration().StateMatrix[SharedService.activeState()];

      $scope.isRecommendableAsset = TransitionHelper.isEligibleForRecommendation(asset);
      $scope.flagLifeTotal = false;
      $scope.updateEnable = configMatrix.asset[asset.getState()].update.apw;

      //disabled the recommendation (gray out - light-bulb) button if it already has the recommendation.
      // As we need to show icons even if tabs is not visible
      $scope.isRecommendedAsset = asset.isRecommendedAsset || false;

      //setting up asset meta which we need to show in APW as minimal asset properties
      updateAssetMetaData();
      if ($scope.isAllTabLoaded) {
        isLocal = true;
        populateAssetTabData(asset);
      }
    }


    /**
     * Check for pricing source and get rate accordingly
     */
    function onPricingSettingUpdate() {
      populatePricingTabData();
      if (isManualAndLocalCurrency) {
        PricingHelperService.populateCostPerPage($scope.dynamicModel);
        PricingHelperService.populateBasePricingData($scope.dynamicModel);
        isManualAndLocalCurrency = false;
      }
      else if (isManualPricing) {
        PricingHelperService.populateCostPerPage($scope.dynamicModel, 1);
        PricingHelperService.populateBasePricingData($scope.dynamicModel, 1);
      }
      if (isManualPricing && isLocalCurrency) {
        isManualAndLocalCurrency = true;
      }
    }


    function getCurrentCountry(isLocalCurrency) {
      if (!isLocalCurrency) {
        $scope.selectedCurrencyCode = Configuration.currencyOptions.USD;
        return;
      }
      var countryInfo = SharedService.getCurrentLevel(LevelType.COUNTRY),
          countryDetails = SharedService.getRegionFromCountrySet(countryInfo.name);

      return countryDetails;
    }

    /**
     * Updates currency when country is changed in breadcrumb
     */
    function onLevelUpdate() {
      populatePricingTabData();

    }

    /**
     * callback to be executed when asset plotted event is triggered.
     * @param $event broad casted asset plot event.
     * @param asset plotted asset. It can be from grid or canvas.
     * */
    function onAssetMap($event, asset) {
      //to remove autoSearchItem of plotted asset.
      appendZoneName(asset);
      removeAutoSearchItem(asset);
    }

    /**
     * Unselect the selected asset on canvas
     */
    function unSelectAsset() {
      safeApply($scope, function () {
        $scope.selectedAsset = null;
        tempAssignee = null;
      });

      // ToDo: Keeping this for future reference
      /*SharedService.removeSelectedAssetHighlights();
       $scope.selectedAsset = null;
       tempAssignee = null;
       SharedService.selectedAsset(null);*/
    }

    /**
     * Initializes data set required for autocomplete fields.
     * @param {boolean} flag true then we have to populate make values to dataSet otherwise will ignore the make initialization.
     */
    function initializeDataSet(flag) {
      //If flag will be true then we have to get all the make from deviceDataSet otherwise need not to push make into dataset.
      //to prevent duplicate entry in dataSet.make array.
      if (flag) {
        Array.prototype.push.apply($scope.dataSet.make, DeviceDataSetService.getMakes());
        Array.prototype.push.apply($scope.dataSet.productNumber, DeviceDataSetService.getProductNumbers());
      }
      searchFields.forEach(function (field) {
        Array.prototype.push.apply($scope.dataSet[field], AssetService.getSet(field));
      });
    }

    /**
     * Get exchange rates from backend
     */
    function getCurrencyExchangeRate() {
      exchangeRates = SharedService.currencyRatesData();
      if (!exchangeRates) {
        //NotifierService.error('UNABLE_TO_GET_CURRENCY_CONVERSION_RATES');
        $log.error("Unable to get currency conversion rates");
        return;
      }
    }

    /**
     * Cb executes when asset is imported.
     * @param event
     * @param assets array of imported assets.
     */
    function onImportedAsset(event, assets) {
      initializeDataSet(false);
    }


    /**
     * Event will be triggered when Data refresh will be completed and we will have all the data in UI
     * @param $event
     * @param isNewProject
     */
    function onRefreshComplete($event, isNewProject) {
      onLoad();
      // We need not do any thing for newely created project
      if (isNewProject) {
        return;
      }
      getCurrencyExchangeRate();
    }

    function onMakeSelect(event, ui) {
      // if OnSelect Event is triggered then copy the value which will help in disabling the blur event
      if (ui && ui.item.value) {
        onSelectEventData = ui.item.value;
      }
      $scope.dynamicModel.make = (ui && ui.item.value) || $scope.dynamicModel.make;
      if ($scope.dynamicModel.make) {
        isAssetSelect = false;
        isProductNumberChange = false;
        populateModel($scope.dynamicModel.make);
        // if model is empty than no need to populate the properties
        if ($scope.dynamicModel.make && $scope.dynamicModel.model) {
          populateDeviceDataSetPropertiesWhenDataChanged(false);
          populateAccessoriesData();
        }
      }
    }

    function onModelSelect(event, ui) {
      // if OnSelect Event is triggered then copy the value which will help in disabling the blur event
      if (ui && ui.item.value) {
        onSelectEventData = ui.item.value;
      }
      $scope.dynamicModel.model = (ui && ui.item.value) || $scope.dynamicModel.model;
      isAssetSelect = false;
      isProductNumberChange = false;
      if ($scope.dynamicModel.model && $scope.dynamicModel.make) {
        populateDeviceDataSetPropertiesWhenDataChanged(false);
        populateAccessoriesData();
      }
    }

    function onProductNumberSelect(event, ui) {
      // if OnSelect Event is triggered then copy the value which will help in disabling the blur event
      if (ui && ui.item.value) {
        onSelectEventData = ui.item.value;
      }
      $scope.dynamicModel.productNumber = (ui && ui.item.value) || $scope.dynamicModel.productNumber;
      if ($scope.dynamicModel.productNumber) {
        isAssetSelect = false;
        isProductNumberChange = true;
        populateDeviceDataSetPropertiesWhenDataChanged(true);
        populateAccessoriesData();
        if (!$scope.dynamicModel.model) {
          $scope.dynamicModel[AssetProperties.CONFIDENCE] = CONFIDENCE.LOW;
        }
      }
    }

    /**
     * This callback function is to get asset which is selected from drop-down list and
     * populate asset Property window.
     * @param key of asset property.
     * */
    function getAutoSearchCb(key) {
      return function (event, ui) {
        var asset;
        // if OnSelect Event is triggered then copy the value which will help in disabling the blur event
        if (ui && ui.item.value) {
          onSelectEventData = ui.item.value;
        }
        $scope.dynamicModel[key] = (ui && ui.item.value) || $scope.dynamicModel[key];
        //To get asset based on given key (assetNumber,serialNumber..)
        if (Utils.isEmptyString($scope.dynamicModel[key])) {
          return;
        }
        asset = AssetService.getAssetBy(key, $scope.dynamicModel[key]);
        if (!asset) {
          return;
        }

        if (asset.plotted && AssetService.isAssetPlacedOnFloor(asset.getId())) { //To check whether the asset is placed on floor or not
          //found asset might be plotted. So need not to do anything.
          $log.debug('Already plotted asset\'s "' + key + '" is assigned to empty asset.');
          return;
        }

        // if the asset is unknown and has outcome replace with existing than check for faulty country
        if (asset.productNumber && isValidForSupportedCountry($scope.selectedAsset)) {
          var dataSet = DeviceDataSetService.getDeviceDataSetFromProductNumber(asset.productNumber);
          if (dataSet && !SharedService.isSkuPresent(dataSet)) {
            NotifierService.warning('THIS_DEVICE_IS_UNSUPPORTED_IN_CURRENT_COUNTRY');
          }
        }

        if (SharedService.isManual($scope.selectedAsset)) {
          populateAssetPropertiesBy(asset, key);
        } else {
          populateAssetProperties(asset);
        }
        populateAccessoriesData();
      };
    }

    function getSelectCb(id) {
      var selectFn;
      switch (id) {
        case AssetProperties.MAKE:
          selectFn = onMakeSelect;
          break;
        case AssetProperties.MODEL:
          selectFn = onModelSelect;
          break;
        case AssetProperties.ASSET_NUMBER:
          selectFn = getAutoSearchCb(id);
          break;
        case AssetProperties.IP_ADDRESS:
          selectFn = getAutoSearchCb(id);
          break;
        case AssetProperties.MAC_ADDRESS:
          selectFn = getAutoSearchCb(id);
          break;
        case AssetProperties.HOST_NAME:
          selectFn = getAutoSearchCb(id);
          break;
        case AssetProperties.PRODUCT_NUMBER:
          selectFn = onProductNumberSelect;
          break;
        case AssetProperties.SERIAL_NUMBER:
          selectFn = getAutoSearchCb(id);
          break;
        default:
          $log.warn('Select callback for auto complete field "' + id
              + '" is not provided. It might cause issue with rendering.');
          //TODO: remove this empty function when 'gateway','mapid' and subnet are not auto complete.
          selectFn = function () {

          };
          break;
      }
      return selectFn;
    }

    /**
     * Function triggered when click on model look up button labeled (Match) from APW.
     * In this we update the asset with original make & original model.
     */
    function onModelLookup() {
      //TODO:remove first condition and check the root cause of CAR-7694
      // selected asset is not there then, no need to populate.
      if ($rootScope.hasViewPermission || !$scope.selectedAsset) {
        return;
      }

      if (!($scope.dynamicModel.originalMake && $scope.dynamicModel.originalModel)) {
        NotifierService.info('PLEASE_ENTER_MAKE_AND_MODEL');
        return;
      }

      // updating with HP Master data.
      bindAssetDataWithHpMasterData($scope.selectedAsset);
    }

    /**
     * Check if hasColorProf property of asset has been changed
     * @param request
     */
    function checkRequestHasColorProf(request) {
      var hasColor, str, mono = 'Mono', color = 'Color';

      //check if hasColorProf property is true, then make hasColor also true.
      if (request.hasOwnProperty(AssetProperties.HAS_COLOR_PROF) && request.hasColorProf) {
        hasColor = DeviceColorMap[$scope.dynamicModel.deviceType];
        if (!Utils.isUndefined(hasColor) && hasColor !== request.hasColorProf) {
          request.hasColor = true;
          str = hasColor ? color : mono;
          request.deviceType = $scope.dynamicModel.deviceType.replace(str, str === mono ? color : mono);
        }
      }
      //if hasColor is made false and hasColorProf is still true then make it false.
      if (request.hasOwnProperty(AssetProperties.HAS_COLOR) && !request.hasColor && $scope.dynamicModel.hasColorProf) {
        request.hasColorProf = false;
      }
    }

    /**
     * Called when device type of an asset is changed
     * @param request
     */
    function changeDeviceType(request) {
      // we don't need to play with device type/ hasColor
      // when make and modal changed or when we assign asset from keyesearch
      // that hasColor value is again changed by user before saving asset properties.
      if (isHasColorChanged && !$scope.dynamicModel.hasColor) {
        isHasColorChanged = false;
        return;
      }
      var deviceType, hasColor, mono = 'Mono', color = 'Color', str;

      //Device Type Change
      if (request.hasOwnProperty(AssetProperties.DEVICE_TYPE)) {
        deviceType = request.deviceType;
        hasColor = DeviceColorMap[deviceType];
        if (hasColor !== undefined) {
          request.hasColor = hasColor;
        }
        //set flag true if device-type is changed
        SharedService.changedDevice($scope.selectedAsset._id);
      } else if (request.hasOwnProperty(AssetProperties.HAS_COLOR)) {
        hasColor = DeviceColorMap[$scope.dynamicModel.deviceType];
        if (!Utils.isUndefined(hasColor) && hasColor !== request.hasColor) {
          str = hasColor ? color : mono;
          request.deviceType = $scope.dynamicModel.deviceType.replace(str, str === mono ? color : mono);
        }
      } else {
        SharedService.changedDevice(null);
      }
      checkRequestHasColorProf(request);
      isHasColorChanged = false;
    }

    function onLevelSelect(event, args) {
      //Whenever any level is switch we have to reset APW and close the left pane
      unSelectAsset();
      SharedService.getSplitter().leftPane.collapse();
    }

    /**
     * Updating asset meta data which we show in apw by default.
     * i.e. minimum asset properties
     */
    function updateAssetMetaData() {
      var selectedAsset = SharedService.selectedAsset() || {}, deviceType = '';
      $scope.assetMetaData = {};
      // Setting up minimal asset data to show in APW
      selectedAsset = selectedAsset.assetData ? selectedAsset.assetData.properties : selectedAsset;
      deviceType = selectedAsset.deviceType || deviceType;
      if (SharedService.labelSettings() && SharedService.labelSettings().show &&
          !SharedService.isTrivialAsset(deviceType)) {
        $scope.assetMetaData = SharedService.labelSettings().show;
      }
    }

    /**
     * Call back for label setting update where we are updating asset meta data which we show in apw by default
     * i.e. minimum asset properties
     * @param event
     */
    function onLabelSettingUpdate(event) {
      updateAssetMetaData();
    }

    /**
     * It assigns the asset to the manual asset. It indeed deletes the manual asset and
     * it's items(level entity map, page volume transfer, outcomes etc.) and plot the
     * the asset to be assigned with the updated properties.
     * @param assignee
     */
    function assignAsset(assignee) {
      var request;
      if (isReplaceWithUnknown) {
        request = RequestBuilder.getUnknownAssetOutcomeDeleteRequest(assignee, $scope.selectedAsset, $scope.dynamicModel);
      } else {
        request = RequestBuilder.getAssetAssignRequest(assignee, $scope.selectedAsset, $scope.dynamicModel);
      }
      // As of now, we only have limited properties in device data set,
      // so no need to clear those properties
      //cleanUpDeviceDataSetProperties(request.assigneeUpdates);
      cleanUpDispositionDataSetProperties(request.assigneeUpdates);
      cleanUpButtonModelData(request.assigneeUpdates);

      // need to update has color property if assignee has color
      if (assignee.hasColor !== null) {
        request.assigneeUpdates.hasColor = assignee.hasColor;
        isHasColorChanged = true;
      }
      changeDeviceType(request.assigneeUpdates);

      // adding mapID
      if (!Utils.isUndefined(assignee.mapID)) {
        request.assigneeUpdates.mapID = assignee.mapID;
      }

      //if selected Asset is having mapId.Same MapId should overridden to the assign asset
      if ($scope.selectedAsset.mapID) {
        request.assigneeUpdates.mapID = $scope.selectedAsset.mapID;
      }

      $log.debug('Assigning asset...');

      function onSuccess(response) {
        tempAssignee = null;
        SharedService.removeMultipleAssetsHighlights();
        SharedService.setSelectedAssets(null);
        $scope.selectedAsset = response.assignedAsset;
        SharedService.selectedAsset(response.assignedAsset);
        $scope.isRecommendableAsset = TransitionHelper.isEligibleForRecommendation($scope.selectedAsset);
        NotifierService.success('ASSET_PROPERTIES_UPDATED_SUCCESSFULLY');
        $rootScope.$broadcast(ViewEvent.ASSET_ASSIGN, response);
        removeAutoSearchItem(response.assignedAsset);
        $log.info('Asset assigned successfully.');
      }

      function onError(error) {
        $log.error('Error while assigning asset.', error);
        //TODO: Localise...
        NotifierService.error('UNABLE_TO_ASSIGN_AN_ASSET');
      }

      //if isReplaceWithUnknown is true then assignment of asset would be for unknown replace asset else for manual asset.
      if (isReplaceWithUnknown) {
        AssetMapService.assignReplaceAsset(request).then(onSuccess, onError);
      } else {
        AssetMapService.assignAsset(request).then(onSuccess, onError);
      }
    }

    /**
     * Return the asset page volume if device type not supported page volume or update the page volume.
     * @param asset request Object not asset
     * @return {{assets: *[]}}
     */
    function getAssetPageVolumeRequest(asset) {
      var assetTransfer, isNotSupportTransfer, isReplacedAssetExcluded, isExcluded, outcome,
          request = {
            assets: [asset]
          };

      outcome = OutcomeService.getOutcomeByAssetId(asset._id);
      if (outcome && outcome.outcome) {
        isReplacedAssetExcluded = TransitionHelper.isReplace(outcome.outcome) ||
            TransitionHelper.isReplaceWithNew(outcome.outcome);
      }
      // ToDo: It's a quick fix as we are not supporting plotted/scanner for page volume.
      // ToDo: also check outcome for sold specific asset.
      // ToDo: But will need to take care printMono, So will need to do it in future.
      isNotSupportTransfer = asset.deviceType === 'plotter' || asset.deviceType === 'scanner' ||
          (asset.deviceType === 'printMono' && !isReplacedAssetExcluded);
      //TODO Remove isReplacedAsset check in next releases when we have replace feature implemented completely
      isExcluded = asset.excludeFromDesign && !isReplacedAssetExcluded;
      // if asset doesn't support page volume then, we need to delete the transfer and update the asset.
      if (isNotSupportTransfer || isExcluded) {
        assetTransfer = RequestBuilder.getTransferDeleteData(AssetService.getAsset(asset._id));
        angular.extend(assetTransfer.asset, asset);
        assetTransfer.affectedAssets.push(assetTransfer.asset);
        request = {
          assets: assetTransfer.affectedAssets,
          transfersIds: assetTransfer.transferIds
        };
      }
      return request;
    }

    /**
     * Callback to be executed when selected asset is updated.
     * @param $event broad casted asset update event.
     * @param assets updated asset. It should be from canvas.
     */
    function onAssetUpdate($event, assets) {
      //if asset is plotted then it should remove asset entry from AutoSearch drop-down.
      assets = assets.length ? assets : [assets];
      assets.forEach(function (asset) {
        if (asset.plotted && $scope.selectedAsset && asset.getId() === $scope.selectedAsset._id) {
          removeAutoSearchItem(asset);
        }
      });
    }

    /**
     * Callback to be executed when selected asset is replotted.
     * @param $event broad casted asset replotted event.
     * @param assets replotted asset. It should be from canvas.
     */
    function onAssetReplot($event, data) {
      removeAutoSearchItem(data.asset);  //if asset is placed then it should be removed from AutoSearch drop-down.
    }

    /**
     * Callback to be executed when add an asset.
     * @param $event broad casted asset update event.
     * @param asset added asset. It should be from canvas.
     */
    function onAssetAdd($event, asset) {
      onAssetSelect($event, asset);
      $scope.flagLifeTotal = false;
    }

    /**
     * Callback to be executed when asset is deleted.
     * @param $event broad casted asset delete event.
     * @param assets List of asset to be deleted.
     */
    function onAssetDelete($event, assets) {
      assets.forEach(function (asset) {
        removeAutoSearchItem(asset);
      });
      AssetService.clearSet(searchFields);
      unSelectAsset();
    }

    /**
     * Callback to be executed when asset reposition is reset.
     * @param $event broad casted reset reposition asset event.
     * @param originalAsset this is require for populating APW after reset asset.
     * @param updatedOutcomeData
     */
    function onOutcomeReset($event, updatedOutcomeData, originalAsset) {
      var asset, canvasAsset,
          ONE = 1,
          THREE = 3,
          resetOutcomes = updatedOutcomeData.resetOutcomes;
      if ($rootScope.token.featureMatrix.oldReset) {
        Object.keys(resetOutcomes).forEach(function (key) {
          canvasAsset = resetOutcomes[key].canvasAsset;
          asset = CanvasAsset.prototype.isCanvasAsset(canvasAsset) ? canvasAsset.getAssetProperties() : canvasAsset;
          // reset the outcome if outcome type is reposition or asset is virtual
          if (TransitionHelper.isReposition(resetOutcomes[key].outcome) || canvasAsset.isVirtual()) {
            removeAutoSearchItem(asset);
          } else if (canvasAsset.isReal() && resetOutcomes[key].isReplacedWithExisting) {
            //add the asset for key search
            addAutoSearchItem(asset);
          } else {
            //to reset rest of the outcome and display the updated asset properties in APW and Master table.
            safeApply($scope, function () {
              $scope.setActiveTab(0);
              populateAssetProperties(asset);
            });
          }
        });
        //if reposition reset happen then populate asset properties based on original asset.
        if (arguments.length === THREE) {
          if (!originalAsset) {
            unSelectAsset();
            SharedService.getSplitter().leftPane.collapse();
            SharedService.selectedAsset(null);
          } else {
            populateSelectedAsset(originalAsset.getAssetProperties());
            SharedService.selectedAsset(originalAsset);
            originalAsset.highlightAsset();
          }
          return;
        }
        //this condition will execute in case of single reset.
        if (Object.keys(resetOutcomes).length === ONE) {
          populateSelectedAsset(asset);
          SharedService.selectedAsset(asset);
        } else {
          unSelectAsset();
          SharedService.getSplitter().leftPane.collapse();
          SharedService.selectedAsset(null);
        }

      } else {
        var updatedAssets = updatedOutcomeData.deleted.assets.concat(updatedOutcomeData.updated.levelEntityMap);
        for (var index = 0; index < updatedAssets.length; index++) {
          canvasAsset = updatedAssets[index];
          asset = CanvasAsset.prototype.isCanvasAsset(canvasAsset) ? canvasAsset.getAssetProperties() : canvasAsset;
          if (TransitionHelper.isReposition(canvasAsset.getOutcome()) || canvasAsset.isVirtual()) {
            removeAutoSearchItem(asset);
          } else if (canvasAsset.isReal() && canvasAsset.isReplacedWithExisting) {
            //add the asset for key search
            addAutoSearchItem(asset);
          }
        }
        //this condition will execute in case of single reset.
        if (updatedOutcomeData.updated.assets && updatedOutcomeData.updated.assets.length === 1) {
          canvasAsset = StageHelper.getCanvasAsset(updatedOutcomeData.updated.assets[0]._id);
          asset = updatedOutcomeData.updated.assets[0];
          //this is to populate accessory on reset recommendation
          if ($rootScope.token.featureMatrix.accessory) {
            $scope.accessoriesData = DeviceAccessoriesService.populateAccessoriesByAsset(asset);
          }
          SharedService.removeMultipleAssetsHighlights();
          SharedService.selectedAsset(canvasAsset || asset);
          populateSelectedAsset(asset);
          if (canvasAsset) {
            canvasAsset.highlightAsset();
          }
        } else {
          SharedService.removeMultipleAssetsHighlights();
          unSelectAsset();
          SharedService.selectedAsset(null);
          SharedService.getSplitter().leftPane.collapse();
        }
      }
    }

    /**
     * Callback to be executed when outcome apply event is triggered.
     * @param $event broad casted asset select event.
     * @param assets {@code Asset} array of Assets
     */
    function onOutcomeApply($event, data) {
      var assets = data.assets;
      safeApply($scope, function () {
        assets = angular.isArray(assets) ? assets : [assets];
        // for populating the asset data.
        assets.forEach(function (asset) {
          var selectedAsset = CanvasAsset.prototype.isCanvasAsset(asset) ? AssetService.getAsset(asset.getId()) : asset;
          $scope.setActiveTab(0);
          // copy the updated asset properties into selected asset for populating the values.
          if (TransitionHelper.isExcludeFromDesign(selectedAsset.getOutcome())) {
            $scope.selectedAsset = angular.copy(selectedAsset);
          }
          populateAssetProperties(selectedAsset);
        });

      });
    }

    /**
     * Callback to be executed when transfer update event is triggered.
     * @param $event broad casted asset select event.
     */
    function onTransferUpdate($event, assets) {
      // in case floor with empty plotted asset.
      if (!$scope.selectedAsset) {
        return;
      }
      var asset = assets.find(function (asset) {
        return asset.getId() === $scope.selectedAsset.getId();
      });
      // copy the updated asset properties into selected asset for populating the values.
      $scope.selectedAsset = angular.copy(asset) || $scope.selectedAsset;
      populateAssetProperties(asset);
    }

    /**
     * When we change the state then this function will call and reset the APW.
     * @param {$event} state which we selected.
     * */
    function onStateChange($event) {
      unSelectAsset();
      SharedService.getSplitter().leftPane.collapse();
    }

    /**
     * This callback will call when asset moved to out-of-scope in master grid.It will close the left pane and
     * UnSelect the selected asset in APW
     * */
    function onAssetMove($event) {
      unSelectAsset();
      SharedService.getSplitter().leftPane.collapse();
    }

    /**
     * Function triggered when click on match to all button labeled [Match (All)] from APW.
     * In this we update the assets with make & model.
     */
    function onApplyToAll() {
      //TODO:remove first condition and check the root cause of CAR-7694
      //if user has view permission return
      if ($rootScope.hasViewPermission) {
        return;
      }
      if ($scope.dynamicModel.originalMake && $scope.dynamicModel.originalModel) {
        $scope.isConfirmApplyToAllVisible = true;
      } else {
        NotifierService.info('PLEASE_ENTER_MAKE_AND_MODEL');
      }
    }


    /**
     * function to update future state location preference in APW when transition state asset is dragged.
     * @returns {boolean}
     */
    function onAssetDragEnd($event, entityMap) {
      if (entityMap && entityMap[0].replacementAsset) {
        $scope.dynamicModel.replacementAsset = entityMap[0].replacementAsset;
      }
    }

    /**
     * function to update zone name when asset is dragged in/out from the zone.
     * @returns {boolean}
     */
    function onAssetZoneUpdated($event, entityMap) {
      var entity = entityMap.first(),
          assetId = '';
      if (entity.hasOwnProperty('outcome') && entity.outcome === 'reposition') {
        assetId = entity.assetId;//this is the id of asset which has outcome
      } else {
        assetId = entity.entityId;// this is the actual asset id of lem
      }
      if (assetId) {
        var asset = AssetService.getAsset(assetId);
        appendZoneName(asset);
      }
    }

    function onZoneAssetsUpdated($event) {
      var levelId = SharedService.getCurrentLevel(LevelType.FLOOR)._id, asset,
          entityMaps = LevelEntityMapService.getLevelEntityMapsByEntityType(levelId, 'asset');
      var assetId = '';
      for (var index = 0; index < entityMaps.length; index++) {
        if (entityMaps[index].hasOwnProperty('outcome') && entityMaps[index].outcome === 'reposition') {
          assetId = entityMaps[index].assetId;//this is the id of asset which has outcome
        } else {
          assetId = entityMaps[index].entityId;// this is the actual asset id of lem
        }
        if (assetId) {
          var asset = AssetService.getAsset(assetId);
          appendZoneName(asset);
        }
      }
    }


    /**
     * function returns true or false based on active state, asset state and outcome of asset
     * @param activeState
     * @param assetState
     * @param outcome
     * @returns {boolean}
     */
    function isShowReplacementAssetDropDown(activeState, assetState, outcome) {
      return !!SharedService.getConfiguration().StateMatrix[activeState].asset[assetState].replacementAsset.outcome[outcome];
    }

    //Required scope variable and methods for tabs asset property window
    $scope.activeTab = 0; //first tab is by active default.

    //currently selected asset.
    $scope.selectedAsset = null;

    // Asset view returned from HtmlFactory
    $scope.assetView = {
      tabs: '',
      tabContent: ''
    };

    // Data required by auto complete fields
    $scope.dataSet = {
      assetNumber: [],
      iPAddress: [],
      hostName: [],
      macAddress: [],
      make: [],
      model: [],
      serialNumber: [],
      productNumber: []
    };

    // Dynamic model will have properties of asset. This model is bound to asset property window.
    // Changes in this model will be reflected in asset property window. It should have deep copy of
    // asset properties so that when user interact with asset properties weon do not loose original data.
    $scope.dynamicModel = {};
    $scope.pricingSource = Configuration.pricingSources.Manual;
    $scope.flagLifeTotal = false;
    $scope.isDcaReadingDate = false;

    //adding trivial function to scope so it can be accessed by views.
    $scope.isTrivial = SharedService.isTrivialAsset;

    //adding trivial function to scope so it can be accessed by views.
    $scope.isVirtual = SharedService.isVirtualAsset;

    //To show minimal property in APW
    $scope.assetMetaData = {};

    /**
     * function to enable disable basket icon apw window.
     * @returns {boolean}
     */
    $scope.showBasketIcon = function () {
      var stateMatrix = SharedService.getConfiguration().StateMatrix || {},
          config = stateMatrix[SharedService.activeState()],
          asset = SharedService.selectedAsset();
      return config && asset &&
          (config.asset[asset.getState()].show.basket.outcome[asset.getOutcome()] === "true");
    };

    $scope.onSelect = getSelectCb;
    $scope.selectedCurrencyCode = Configuration.currencyOptions.USD;

    /**
     *  Disable exclude from design checkbox for virtual asset
     *  @param field name from asset property window
     */
    $scope.disableExcludeFromDesignCheck = function (field) {
      if ($scope.selectedAsset) {
        return field === AssetProperties.EXCLUDE_FROM_DESIGN && $scope.selectedAsset.isVirtual();
      }
    };

    /**
     *  Disable the text box from APW
     *  @param field is the key for dynamic model from asset property window
     */
    $scope.disableBox = function (field) {
      var isDisabled = false;
      // disabling text box for empty asset.
      if ($scope.dynamicModel) {
        switch (field) {
            //TODO can be add more cases based on requirement.
          case AssetProperties.MAP_ID:
            isDisabled = !($scope.dynamicModel.make && $scope.dynamicModel.model);
            break;

          case AssetProperties.MODEL:
            isDisabled = !$scope.dynamicModel.make;
            break;

          case AssetProperties.SUB_DISPOSITIONS:
            isDisabled = $scope.dynamicModel.outcome !== OutcomeType.RETAIN;
            break;
          case AssetProperties.WARNING:
            isDisabled = true;
            break;
          case AssetProperties.MONO_A3_MONTHLY:
          case AssetProperties.COLOR_A3_MONTHLY:
          case AssetProperties.MONO_A3_LAST_READING:
          case AssetProperties.COLOR_A3_LAST_READING:
            isDisabled = !$scope.dynamicModel.hasA3;
            break;
          case AssetProperties.COLOR_PRO_LAST_READING:
          case AssetProperties.COLOR_PRO_MONTHLY_VOLUME:
          case AssetProperties.COLOR_PRO_A4_LAST_READING:
          case AssetProperties.COLOR_PRO_A4_MONTHLY:
          case AssetProperties.COLOR_PRO_A3_LAST_READING:
          case AssetProperties.COLOR_PRO_A3_MONTHLY:
            isDisabled = !$scope.dynamicModel.hasColorProf;
            break;
        }
      }
      return isDisabled;
    };
    /**
     *  Show the dropdown in APW
     *  @param fieldKey
     */
    $scope.isShowSelectDropDown = function (fieldKey) {
      switch (fieldKey) {
        case Configuration.replacementAsset:
          var activeState = SharedService.activeState(),
              assetState = $scope.selectedAsset ? $scope.selectedAsset.getState() : Configuration.ASSET_STATES.REAL,
              outcomeVal = $scope.selectedAsset ? $scope.selectedAsset.getOutcome() : 'empty';
          return isShowReplacementAssetDropDown(activeState, assetState, outcomeVal);
        default:
          return false;
      }
    };
    /**
     *  function callback on select change of drop down
     *  @param fieldKey, field of dynamic model
     */
    $scope.onSelectChange = function (fieldKey, field) {
      switch (fieldKey) {
        case Configuration.replacementAsset:
          AssetPlacementHelper.switchAssetLocation(field, $scope.selectedAsset);
          break;
        default:
          break;
      }
    };

    /**
     * Sets the active tab in asset property window.
     * @param tabId id of the tab to be activated.
     */
    $scope.setActiveTab = function (tabId) {
      $scope.activeTab = tabId;
    };

    /**
     * Function triggered when click on any inside button from APW.
     * It is mainly determine the callback based on the button id.
     * @param id
     */
    $scope.onButtonClick = function (id) {
      switch (id) {
        case BUTTON_MODEL.MODEL_LOOKUP:
          onModelLookup();
          break;
        case BUTTON_MODEL.APPLY_TO_ALL:
          onApplyToAll();
          break;
      }
    };
    /**
     * Called when Cpp fiels gets blur.
     * @param key , key of the field which is blurred.
     */
    $scope.onCppblur = function (key) {
      $scope.dynamicModel[key] = Utils.toFloat($scope.dynamicModel[key] || 0, Configuration.PRECISION_FIVE);
    };

    /**
     * onBlur Callback for auto complete field.
     * @param id, id of the field which is focused.
     */
    $scope.onAutoCompleteBlur = function (id) {

      // we are calling the onSelect Event so if the same value is present then no need to trigger autoBlur event
      if ($scope.dynamicModel[id] === $scope.selectedAsset[id] || onSelectEventData === $scope.dynamicModel[id]) {
        onSelectEventData = '';
        return;
      }

      // When element is selected ultimately we need to make decision
      // based on the value entered in input box. Which is same as what
      // we do when user selects the value. So calling corresponding cb.
      var selectCb = getSelectCb(id);
      selectCb();
      safeApply($scope);
    };

    /**
     * This function will mainly open the popup by broadcast
     * @param {String} popUpName Name of popup.
     */
    $scope.popupFlag = function (popUpName) {
      $rootScope.$broadcast(ViewEvent.UPLOAD_ASSET_ATTACHMENT, popUpName, SharedService.selectedAsset());
    };

    /**
     * Whenever we unmap an asset there are so many things regarding to an asset changes.
     * For example we need to map the asset to project instead of floor and update the coordinates
     * in level entity map to null. We need to update the asset itself to un-plotted. There are so many
     * stuff like this. Function {@code $scope.unmapAsset} makes sure that all of these things are handled
     * properly.
     */
    $scope.unmapAsset = function () {
      var config = SharedService.getConfiguration().StateMatrix[SharedService.activeState()];

      if (!config.asset.unplotAsset) {
        NotifierService.info('UNPLOTTING_IS_NOT_ALLOWED_IN_TRASITION_FUTURE_STATE');
        return;
      }

      var flag, request, outcomeObj, canvasAsset;
      flag = SharedService.isPlottedOnCurrentFloor($scope.selectedAsset.getId());

      // 'isFloorMapView' will check that unplotting is done from tabular view or floor map view.
      // In case of tabular view, unplotting can be done for any asset on any floor.
      if (!flag && $rootScope.isFloorMapView) {
        NotifierService.info('PLEASE_SELECT_AN_ASSET_ON_THE_MAP_TO_UNPLOT');
        $log.info('Tried to un-plot the asset which is not mapped.');
        return;
      }

      flag = $window.confirm($filter('translate')('ARE_YOU_SURE_YOU_WANT_TO_UNMAP_THIS_ASSET?'));
      if (!flag) {
        $log.info('Asset un-plot dialog cancelled.');
        return;
      }

      request = RequestBuilder.getAssetUnmapRequests($scope.selectedAsset);

      function onUnMapError(error) {
        var message = error.customError ? error.message : 'ERROR_WHILE_UNMAPPING_ASSET';
        NotifierService.error(message);
        $log.error('Error while un-mapping asset.');
        $log.error('Something went wrong in deletion of transfer related to unmap asset.');
        $log.error(error);

      }

      function onUnMapSuccess(response) {
        var assetsTransfers, unmapAssetIds = [],
            leftPane = SharedService.getSplitter().leftPane;

        //Add the real replace asset to Remove from canvas
        function forEachCb(assetId) {
          unmapAssetIds.push(assetId);
        }

        // check transfers is having with the selected asset.
        if (response.transfersIds && response.transfersIds.length) {
          assetsTransfers = {
            transfersIds: response.transfersIds,
            assets: response.assets
          };
          // broadcasting the transfer delete event
          $rootScope.$broadcast(ViewEvent.TRANSFER_DELETE, assetsTransfers);
        }
        //ToDo: Need to do something with {{response.outcome}} if required.
        // Todo: Need to change when backend return deleted id's instead of counter.
        //add auto search values to auto complete list.
        addAutoSearchItem($scope.selectedAsset);

        if (response.repositionOutcomes) {
          SharedService.removeAssetsFromCanvas(response.repositionOutcomes.first());
          $log.info('Virtual Asset is removed from canvas');
        }

        request.replacedAssets.forEach(forEachCb);
        // broadcasting the un-map event
        unmapAssetIds.push($scope.selectedAsset.getId());
        $rootScope.$broadcast(ViewEvent.ASSET_UNMAP, unmapAssetIds);

        unSelectAsset();
        leftPane.collapse();
        $log.info('Asset un-mapped successfully.');
      }

      let unPlotRequest = {
        assets: request.assets,
        levelEntityMaps: request.entityMaps,
        outcomes: request.outcomes,
        transfersIds: request.transfersIds,
        repositionOutcome: request.repositionOutcome,
        replacedAssets: request.replacedAssets
      };
      $log.debug('Un-mapping asset...');
      AssetMapHandler.unPlotAsset(unPlotRequest).then(onUnMapSuccess, onUnMapError);
    };


    /**
     * Poulate Tab data based on tab id of clicked tab
     * @param tabId
     */
    $scope.showTabInfo = function (tabId) {
      $scope.activeTab = tabId;
      $scope.assetView = HtmlFactory.getTabContent(tabId, $scope.selectedAsset.deviceType);
    };

    /**
     * Responsible to update FMO value of accessories based on cmo, this function will be called
     * when user changes CMO of accessory
     * @param accessoriesObj
     */
    $scope.onCMOChange = function (accessoriesObj) {
      accessoriesObj.FMO = accessoriesObj.CMO;
    };

    /**
     * function to update other related Asset fields
     * @param changes
     */
    function updateRelatedFields(changes) {
      // to sending other fields in the asset update request those are needed to perform some calculation on backend.
      if (changes.monoA3Monthly || changes.monoA3Monthly === 0) {
        changes.colorA3Monthly = $scope.dynamicModel.colorA3Monthly;
        if (!changes.hasOwnProperty('monoTotalMonthly')) {
          changes.monoTotalMonthly = parseInt(Utils.toNumberWithZero(changes.monoA3Monthly) + Utils.toNumberWithZero($scope.dynamicModel.monoA4Monthly));
          $scope.dynamicModel.monoTotalMonthly = changes.monoTotalMonthly;
          changes.proposedMonthlyPageVolumeMono =
              ($scope.dynamicModel.proposedMonthlyPageVolumeMono -
                  Utils.toNumberWithZero($scope.selectedAsset.monoTotalMonthly)) + Utils.toNumberWithZero($scope.dynamicModel.monoTotalMonthly);
          $scope.dynamicModel.proposedMonthlyPageVolumeMono = changes.proposedMonthlyPageVolumeMono;
          changes.proposedMonthlyPageVolumeMonoA3 =
              ($scope.dynamicModel.proposedMonthlyPageVolumeMonoA3 -
                  Utils.toNumberWithZero($scope.selectedAsset.monoA3Monthly)) + Utils.toNumberWithZero($scope.dynamicModel.monoA3Monthly);
        }
      }

      if (changes.colorA3Monthly || changes.colorA3Monthly === 0) {
        changes.monoA3Monthly = $scope.dynamicModel.monoA3Monthly;
        if (!changes.hasOwnProperty('colorTotalMonthly')) {
          changes.colorTotalMonthly = parseInt(Utils.toNumberWithZero(changes.colorA3Monthly) + Utils.toNumberWithZero($scope.dynamicModel.colorA4Monthly));
          $scope.dynamicModel.colorTotalMonthly = changes.colorTotalMonthly;
          changes.proposedMonthlyPageVolumeColor =
              ($scope.selectedAsset.proposedMonthlyPageVolumeColor -
                  Utils.toNumberWithZero($scope.selectedAsset.colorTotalMonthly)) + Utils.toNumberWithZero($scope.dynamicModel.colorTotalMonthly);
          $scope.dynamicModel.proposedMonthlyPageVolumeColor = changes.proposedMonthlyPageVolumeColor;
          changes.proposedMonthlyPageVolumeColorA3 =
              ($scope.selectedAsset.proposedMonthlyPageVolumeColorA3 -
                  Utils.toNumberWithZero($scope.selectedAsset.colorA3Monthly)) + Utils.toNumberWithZero($scope.dynamicModel.colorA3Monthly);
        }
      }

      if (changes.monoTotalMonthly || changes.colorTotalMonthly) {
        PageVolumeCalculationService.updateA4Fields($scope.selectedAsset._id, changes, $scope.dynamicModel);
      }

      if ((changes.hasOwnProperty('rmpv') && changes.rmpv) || (changes.hasOwnProperty('engineLife') && changes.engineLife)) {
        changes.rmpv = $scope.dynamicModel.rmpv;
        changes.engineLife = $scope.dynamicModel.engineLife;
        changes.monoTotalMonthly = $scope.dynamicModel.monoTotalMonthly;
        changes.colorTotalMonthly = $scope.dynamicModel.colorTotalMonthly;
        changes.proposedMonthlyPageVolumeMono = $scope.dynamicModel.proposedMonthlyPageVolumeMono;
        changes.proposedMonthlyPageVolumeColor = $scope.dynamicModel.proposedMonthlyPageVolumeColor;
      }

      if (!changes.dateInstalled) {
        changes.dateManufactured = $scope.dynamicModel.dateManufactured ? $scope.dynamicModel.dateManufactured : '';
        changes.dateIntroduced = $scope.dynamicModel.dateIntroduced ? $scope.dynamicModel.dateIntroduced : '';
        changes.dateInstalled = $scope.dynamicModel.dateInstalled ? $scope.dynamicModel.dateInstalled : '';
      }
    }

    /**
     * changes the ampv calculation method to not found if the respective dates are deleted.
     * @param changes
     * @param method
     */
    function updateAmpvCalculationMethod(changes, method) {
      if ($scope.dynamicModel.ampvCalculationMethod === method) {
        changes.ampvCalculationMethod = Configuration.ampvCalculationMethod.notFound;
      }
    }

    /**
     * As name suggest it saves the asset properties. It makes sure that only updated values are
     * send in request.
     * @param callback function if anything we want to do further after success callback.
     */
    $scope.saveAssetProperties = function (callback) {
      var selectedAsset, updateOutcomeReq, changes, message, isReplaceWithExisting, isMakeModelChange,
          isRecommendedAsset, isNeedEnumeratedMapId = false,
          isUnknownState = $scope.selectedAsset.isUnknownState(), lcExchangeRate,
          request = {
            _id: $scope.selectedAsset.getId()
          };
      isAssetSelect = false;
      isProductNumberChange = false;
      // check for assign asset
      if (tempAssignee) {
        isNeedEnumeratedMapId = ((tempAssignee.make && tempAssignee.model) && !(isReplaceWithUnknown || tempAssignee.mapID));
        enumeratedMapId(tempAssignee, isNeedEnumeratedMapId, assignAsset);
        return;
      }

      /**
       * Updated asset callback.
       * @param response {{ assets : [*], transfersIds : [*] }} with updated properties.
       */
      function onUpdateSuccess(response) {
        var asset, assetsTransfers, canvasAsset, accessoriesDelRequest;
        // we get array of asset(s) if requested asset having transfer so we need to find the selected one.
        asset = response.assets.find(function (asset) {
          return asset.getId() === $scope.selectedAsset.getId();
        });
        // update selected asset with updated Asset
        // $scope.selectedAsset = asset;

        if (SharedService.isFloorMapView() && $scope.selectedAsset) {
          var assetMapId = StageHelper.getAssetMapId($scope.selectedAsset._id);
          if (assetMapId) {
            assetMapId.updateLabelFont();
          }
        }

        //Update the outcome
        if (updateOutcomeReq) {
          OutcomeService.updateOutcomes([updateOutcomeReq]).then(function () {
                $log.info('outcome updated');
              },
              function (error) {
                $log.info('error while updating outcome: ', error);
              });
        }
        // check selected assets having the transfers.
        if (response.transfersIds.length) {
          assetsTransfers = {
            transfersIds: response.transfersIds,
            assets: response.assets
          };
          // broadcasting the transfer delete event
          $rootScope.$broadcast(ViewEvent.TRANSFER_DELETE, assetsTransfers);
        }
        safeApply($scope, function () {
          $scope.assetView = HtmlFactory.getAssetView(asset.getType());
          PricingHelperService.changeCurrencyRatesIntoLC(asset, lcExchangeRate);
          //ampv calculation has been calculated already before saving Asset, by setting 2nd parameter true we are
          //preventing  calculation of apmv again
          populateSelectedAsset(asset, true);
          isLocal = false;
        });
        // it trigger the callback function for specific operation.
        if (Utils.isFunction(callback)) {
          callback(asset);
          return;
        }

        /**
         * Asset update this will broadcast asset update event when updateAccessories call executed
         */
        function assetUpdate() {
          $scope.setActiveTab(0); //make first tab i.e. basic tab active
          $rootScope.$broadcast(ViewEvent.ASSET_UPDATE, asset, isManualAssetAssign);
          // Updating Asset Accessories
          // need to reset it again.
          isManualAssetAssign = false;
          NotifierService.success('ASSET_PROPERTIES_UPDATED_SUCCESSFULLY');
          $log.info('Asset updated successfully.');
        }

        function updateAccessories() {
          DeviceAccessoriesService.updateAccessories($scope.accessoriesData).then(function (response) {
            assetUpdate();
            $log.info('accessories updated');
          }, function (error) {
            assetUpdate();
            $log.error('unable to update accessories', error);
          });
        }

        // Note : If SKU changes then, we have to delete the accessories in database and save the new accessories.
        // After saving the accessories, we will send the call for asset update. This calls must be in sequence.
        // Because asset update call broadcast an event to get the latest accessories. This will conflict the accessory data.

        accessoriesDelRequest = {
          assetIds: [asset._id]
        };
        if ($rootScope.token.featureMatrix.accessory) {
          DeviceAccessoriesService.deleteAccessories(accessoriesDelRequest).then(function (response) {
            updateAccessories();
            $log.info('accessories deleted');
          }, function (error) {
            assetUpdate();
            $log.error('unable to delete accessories', error);
          });
        } else {
          assetUpdate();
        }

      }

      /**
       * onError callback if anything goes wrong during asset update.
       * @param error
       */
      function onError(error) {
        var message = error.customError ? error.message : 'ERROR_UPDATING_ASSET_PROPERTIES';
        NotifierService.error(message);
        $log.error('Unable to save asset properties');
        $log.error(error);
        //reverting changes by populating actual data
        populateAssetProperties($scope.selectedAsset);
      }
      //calculate ampv and get changes
      var assetTemp = angular.copy($scope.selectedAsset).getAssetProperties();
      angular.extend(assetTemp, $scope.dynamicModel);
      calculateAmpv(assetTemp);
      // Getting only those changes which are made by user
      changes = Utils.getChanges($scope.selectedAsset, Utils.toPlainObject($scope.dynamicModel));

      // As of now, we only have limited properties in device data set,
      // so no need to clear those properties
      //changes = cleanUpDeviceDataSetProperties(changes);
      changes = cleanUpDispositionDataSetProperties(changes);
      changes = cleanUpButtonModelData(changes);
      // replacing the forbidden characters with ':'.
      Utils.replaceForbiddenCharacters(':', changes, Object.keys(changes));

      // Detect Changes in reading tab and update last reading date
      if (SharedService.hasReadingField(changes)) {
        changes.lastReadingDate = new Date();
        changes.isDcaReadingDate = false;
      }

      if (!$scope.dynamicModel.dateIntroduced) {
        changes.dateIntroduced = '';
        updateAmpvCalculationMethod(changes, Configuration.ampvCalculationMethod.fromDateIntroduced);
      }
      if (!$scope.dynamicModel.dateManufactured) {
        changes.dateManufactured = '';
        updateAmpvCalculationMethod(changes, Configuration.ampvCalculationMethod.fromDateManufactured);
      }

      if (!$scope.dynamicModel.dateInstalled) {
        changes.dateInstalled = '';
        updateAmpvCalculationMethod(changes, Configuration.ampvCalculationMethod.fromDateInstalled);
      }

      function updateLifeTotal(changes) {

        var isLifeTotalMonthlyChanged, isLifeTotalLastReadingChanged, pvtToAssets, pvtFromAssets,
            isTotalValueChanged = changes.lifeTotalMonthly === Math.trunc($scope.selectedAsset.lifeTotalMonthly);
        // check if total value changed manually or its coming from auto calculations
        isLifeTotalMonthlyChanged = parseInt(changes.lifeTotalMonthly) !== parseInt(calculatedTotalMonthlyReading);
        isLifeTotalLastReadingChanged = parseInt(changes.lifeTotalLastReading) !== parseInt(calculatedTotalLastReading);


        // update the manual changed flag for total values and show in red color
        if (changes.lifeTotalLastReading && isLifeTotalLastReadingChanged) {
          changes.isTotalLastReadingManualChange = true;
        }

        // update the manual changed flag for total values and show in red color
        if (changes.lifeTotalMonthly && isLifeTotalMonthlyChanged) {
          changes.isTotalMonthlyManualChange = true;
        }

        // Update monthly total
        function updateTotalMonthly() {
          if (!$scope.selectedAsset.hasTotalMonthly || $scope.selectedAsset.isMonthlyReadingManualChanged) {
            changes.lifeTotalMonthly = Utils.toFloor($scope.dynamicModel.monoTotalMonthly) +
                Utils.toFloor($scope.dynamicModel.colorTotalMonthly) +
                Utils.toFloor($scope.dynamicModel.colorProMonthlyVolume);
            changes.isMonthlyReadingManualChanged = true;
            $scope.flagLifeTotal = true;
            changes.hasTotal = true;
          }
        }

        // Update life total
        function updateTotalLastReading() {
          changes.lifeTotalLastReading = Utils.toFloor($scope.dynamicModel.monoTotalLastReading) +
              Utils.toFloor($scope.dynamicModel.colorTotalLastReading) +
              Utils.toFloor($scope.dynamicModel.colorProLastReading);
          $scope.flagLifeTotal = true;
          changes.hasTotal = true;
          if (!$scope.selectedAsset.hasTotalLastReading || $scope.selectedAsset.isLastReadingManualChanged) {
            changes.isLastReadingManualChanged = true;
          }
        }

        function updateLifeTotalWithDefault() {
          if (changes.monoA3Monthly || changes.monoA3Monthly === 0 || changes.colorA3Monthly || changes.colorA3Monthly === 0) {
            changes.lifeTotalMonthly = $scope.dynamicModel.lifeTotalMonthly;
          }
        }

        if (!isTotalValueChanged && (!changes.lifeTotalMonthly || !$scope.selectedAsset.lifeTotalMonthly) &&
            (changes.hasOwnProperty('monoTotalMonthly') || changes.hasOwnProperty('colorTotalMonthly') || changes.hasOwnProperty('colorProMonthlyVolume'))) {
          updateTotalMonthly();
        }
        else {
          updateLifeTotalWithDefault();
        }
        pvtToAssets = PageVolumeService.getPageVolumesTo($scope.selectedAsset.getId());
        pvtFromAssets = PageVolumeService.getPageVolumesFrom($scope.selectedAsset.getId());
        //check if asset doesn't have any page volume transfer then assign same amount of A3/A4 volumes to respective  proposed volumes
        if (!(pvtToAssets && pvtToAssets.length && pvtFromAssets && pvtFromAssets.length)) {
          if (changes.monoA4Monthly) {
            changes.proposedMonthlyPageVolumeMonoA4 = changes.monoA4Monthly;
          }
          if (changes.monoA3Monthly) {
            changes.proposedMonthlyPageVolumeMonoA3 = changes.monoA3Monthly;
          }
          if (changes.colorA4Monthly) {
            changes.proposedMonthlyPageVolumeColorA4 = changes.colorA4Monthly;
          }
          if (changes.colorA3Monthly) {
            changes.proposedMonthlyPageVolumeColorA3 = changes.colorA3Monthly;
          }
        }

        if (!changes.lifeTotalLastReading || !$scope.selectedAsset.lifeTotalLastReading) {
          if (changes.monoTotalLastReading || changes.colorTotalLastReading) {
            updateTotalLastReading();
          }
        }

      }

      if (!$scope.selectedAsset.isVirtual()) {
        isAssetExcluded(changes);
      }

      updateLifeTotal(changes);

      // calculate the rmpv value while updating the asset
      if ($rootScope.token.featureMatrix.hpMasterData) {
        changes.rmpv = AssetService.getRmpv($scope.dynamicModel);
      }
      updateRelatedFields(changes);
      lcExchangeRate = PricingHelperService.getExchangeRate(exchangeRates, getCurrentCountry(true));
      PricingHelperService.changeCurrencyExchangeRate(lcExchangeRate, changes);

      // Round a reading tab specific numbers downward to its nearest integer
      changes = Utils.toFloorByKeys(changes, getReadingTabProperties());

      message = validateUpdateRequest(changes);
      if (message) {
        NotifierService.info(message);
        return;
      }

      // there's a change in make and model the device data set population happens which
      // actually says asset is not empty. So setting isManual false.
      if (isDeviceDataSetUpdated(changes, $scope.selectedAsset)) {
        changes.isManual = false;
      }

      //add changes to request
      angular.extend(request, changes);
      changeDeviceType(request);

      // re-setting the page volume if device not supported the page volume.
      request = getAssetPageVolumeRequest(request);

      //To find the selected asset on request array.
      selectedAsset = request.assets.find(function (asset, index) {
        return asset._id === $scope.selectedAsset.getId();
      });

      // it update the request properties for replaceWithNew to an Unknown Asset.
      function replaceWithNewAsset() {
        if (selectedAsset) {
          angular.extend(selectedAsset, {
            state: AssetProperties.VIRTUAL_STATE,
            outcome: OutcomeType.REPLACE_WITH_NEW
            //This property is required to update labelText in assetLabel file
            //isAssetUpdatedByMakeAndModel: true
          });
          // to get the outcome of selected asset
          var outcome = OutcomeService.getOutcomeByAssetId(selectedAsset._id);
          updateOutcomeReq = {
            _id: outcome.getId(),
            outcome: OutcomeType.REPLACE_WITH_NEW,
            coordinates: outcome.coordinates ? outcome.coordinates : null
          };
        }
      }

      isMakeModelChange = (changes.make && changes.model) || (changes.originalMake && changes.originalModel);
      isRecommendedAsset = changes.isRecommendedAsset;

      //if user manually changes the make and model then we required this condition
      if (isMakeModelChange && isUnknownState) {
        isManualAssetAssign = true;
      }
      // if user manually changes the make and model or for recommended asset.
      if ((isMakeModelChange || isRecommendedAsset) && isUnknownState) {
        replaceWithNewAsset();
      }

      /**
       * Set deal term to request
       * @param request
       */
      function setDealTerm(request) {
        var projectSettingsData = ProjectSetting.getProjectSettings($routeParams.id);
        if (projectSettingsData && projectSettingsData.dealSettings) {
          request.term = projectSettingsData.dealSettings.term;
        } else {
          request.term = Configuration.DEFAULT_DEAL_TERM;
        }
      }


      /**
       * Set project deployment date to request
       * @param request
       */
      function setProjectDeploymentDate(request) {
        var projectData = LevelService.getLevel($routeParams.id);
        if (projectData && projectData.createdDate) {
          request.deploymentDate = projectData.createdDate;
        }
      }

      // function is responsible for updating asset.
      function updateAsset(request) {
        setDealTerm(request);
        setProjectDeploymentDate(request);
        $log.debug('Updating asset properties...');
        delete request.zone;
        AssetMapService.updateAssets(request).then(onUpdateSuccess, onError);
      }

      // finally check for mapId enumeration before calling update request.
      isReplaceWithExisting = TransitionHelper.isReplaceWithExisting($scope.selectedAsset.getOutcome());
      isNeedEnumeratedMapId = $scope.selectedAsset.isReal() && !isReplaceWithExisting &&
          $scope.dynamicModel.make && $scope.dynamicModel.model && $scope.selectedAsset.plotted;

      if (!Utils.isUndefined(changes.mapID) && isNeedEnumeratedMapId) {
        // just need to update mapId in project setting and next enumeration value.
        angular.extend(selectedAsset, {
          mapID: updateProjectMapId(changes).mapID
        });
        updateAsset(request);
      } else if (!$scope.dynamicModel.mapID && selectedAsset && isNeedEnumeratedMapId && changes.make && changes.model) {
        enumeratedMapId(selectedAsset, isNeedEnumeratedMapId, function (response) {
          angular.extend(selectedAsset, {
            mapID: response.mapID
          });
          updateAsset(request);
        });
      } else {
        updateAsset(request);
      }
    };
    /**
     * calculate ampv and populate
     * @param request assetData
     */

    function calculateAmpv(request) {
      changeDeviceType(request);
      AmpvCalculationService.calculateAMPVandPopulate(request, $scope.dynamicModel);
    }
    /**
     * This Function is responsible for recommending an asset based of ASM response.
     */
    $scope.onRecommendedAsset = function () {
      var recommendedRequest = {};
      recommendedRequest[$scope.selectedAsset._id] = $scope.selectedAsset;
      $rootScope.$broadcast(ViewEvent.RECOMMENDED_ASSET, recommendedRequest, true);
    };


    /**
     * Function is responsible to reset the recommendation.
     * Trigger when user click on reset icon from APW.
     */
    $scope.onResetRecommendedAsset = function () {
      $rootScope.$broadcast(ViewEvent.RESET_RECOMMENDATION, [$scope.selectedAsset]);
    };

    /**
     * This Function is responsible for displaying the basket of goods results(ASM).
     */
    $scope.onBasketOfGoods = function () {
      //If asset accessories are not loaded then don't show basket to user.
      if (!AccessoriesDataMapService.isAccessoriesDataMapLoaded()) {
        NotifierService.notify('PLEASE_WAIT_FOR_ACCESSORIES_TO_LOAD_AND_TRY_AGAIN', NotifierService.WARNING);
      } else {
        $rootScope.$broadcast(ViewEvent.VIEW_BASKET);
      }
    };

    $scope.getAssetIcon = function (type) {
      return IconService.getIcon(type);
    };

    /**
     * to bind the tenant specific outcome values to HTML factory
     * @param outcome
     * @returns {exports.models.OutcomeSet.properties.outcomeLabel|{type, description}}
     */
    $scope.getOutcomeValue = function (outcome) {
      if (outcome) {
        var selectedAsset = SharedService.selectedAsset();
        if (selectedAsset && selectedAsset.getState()) {
          return SharedService.getOutcomeLabel(selectedAsset.getId(), outcome, selectedAsset.getState());
        }
        return SharedService.getOutcomeLabel(null, outcome);
      }
    };

    /**
     * Return the class name based on confidence level.
     * @param value
     * @return {*}
     */
    $scope.getConfidenceClassName = function (value) {
      if (value) {
        value = value.toLowerCase();
        return value.substring(value.lastIndexOf('-') + 1, value.length);
      }
      return '';
    };

    /**
     * Function to  get Device data set when the controller will be loaded
     */
    function onLoad() {
      if (!DeviceDataSetService.isDeviceDataSetLoaded()) {
        DeviceDataSetService.loadDeviceDataSet().then(function () {
          initializeDataSet(true);
        }, function (error) {
          $log.error('unable to update accessories', error);
        });
      } else {
        initializeDataSet(true);
      }

      if ($rootScope.token.featureMatrix.accessory && !AccessoriesDataMapService.isAccessoriesDataMapLoaded()) {
        AccessoriesDataMapService.loadAccessoriesDataMap();
      }
    }


    /**
     * This function will responsible extend the button property into model.
     * @param dynamicModel {Object} in which object we need to extend.
     * @param property which we need to extend into dynamicModel. Property is very specific form, so need to deal more gracefully.
     * @returns {boolean}
     */
    $scope.initButtonProperty = function (dynamicModel, property) {
      property = property.replace(/\`/g, '"');
      property = JSON.parse(property);
      angular.forEach(property, function (value, key) {
        buttonModals[key] = property;
        dynamicModel[key] = property[key];
      });
    };


    //Check iPAddress in dynamic model and update connection type.
    $scope.$watch('dynamicModel.iPAddress', ipAddressWatcher);

    function getWatcher(fn, key) {
      return fn.bind($scope.dynamicModel[key], key);
    }

    /**
     * pricing tab watcher which update pricing tab data on change value.
     */
    $scope.$watch('dynamicModel.currentMonoCPP', getWatcher(populatePricingTabData, Configuration.PRICING.currentMonoCPP));
    $scope.$watch('dynamicModel.currentColorCPP', getWatcher(populatePricingTabData, Configuration.PRICING.currentColorCPP));
    $scope.$watch('dynamicModel.proposedMonoCPP', getWatcher(populatePricingTabData, Configuration.PRICING.proposedMonoCPP));
    $scope.$watch('dynamicModel.proposedColorCPP', getWatcher(populatePricingTabData, Configuration.PRICING.proposedColorCPP));
    $scope.$watch('dynamicModel.currentColorProCPP', getWatcher(populatePricingTabData, Configuration.PRICING.currentColorProCPP));
    $scope.$watch('dynamicModel.proposedColorProCPP', getWatcher(populatePricingTabData, Configuration.PRICING.proposedColorProCPP));
    $scope.$watch('dynamicModel.currentColorProMonthlyVolume', getWatcher(populatePricingTabData, Configuration.PRICING.currentColorProMonthlyVolume));
    $scope.$watch('dynamicModel.proposedColorProMonthlyVolume', getWatcher(populatePricingTabData, Configuration.PRICING.proposedColorProMonthlyVolume));
    $scope.$watch('dynamicModel.currentBaseMonthlyCost', getWatcher(populatePricingTabData, Configuration.PRICING.currentBaseMonthlyCost));
    $scope.$watch('dynamicModel.proposedBaseMonthlyCost', getWatcher(populatePricingTabData, Configuration.PRICING.proposedBaseMonthlyCost));


    $scope.$watch('dynamicModel.make', function (newValue, oldValue) {
      if (newValue === oldValue || isAssetSelect || isProductNumberChange) {
        return;
      }

      // empty model field, if value has changed of make
      $scope.dynamicModel.model = '';
    });

    $scope.$watch('dynamicModel.model', function (newValue, oldValue) {
      if (newValue === oldValue || isAssetSelect) {
        return;
      }

      $scope.dynamicModel[AssetProperties.CONFIDENCE] = $scope.dynamicModel.model ? CONFIDENCE.HIGH : CONFIDENCE.LOW;
    });

    $scope.$watch('dynamicModel.originalMake', function (newValue, oldValue) {
      if (newValue === oldValue && !(isAssetSelect)) {
        return;
      }

      // enabling modelLookup (match) button when user changed in the original make.
      if ($scope.dynamicModel.hasOwnProperty(BUTTON_MODEL.MODEL_LOOKUP)) {
        disableMatchButton($scope.dynamicModel, false);
      }
    });

    $scope.$watch('dynamicModel.originalModel', function (newValue, oldValue) {
      if (newValue === oldValue && !(isAssetSelect)) {
        return;
      }

      // enabling modelLookup (match) button when user changed in the original model.
      if ($scope.dynamicModel.hasOwnProperty(BUTTON_MODEL.MODEL_LOOKUP)) {
        disableMatchButton($scope.dynamicModel, false);
      }
    });

    /**
     * To get dart pricing on APW
     * @param $event
     * @param source
     * @param dartPricing
     */
    function getPricing($event, source, dartPricing) {
      if (dartPricing && dartPricing.length && source === Configuration.pricingSourcesHp.DART) {
        PricingHelperService.assetDartPricingData(dartPricing);
      }
      populatePricingTabData();
    }

    /**
     * Get pricing Model based on pricing source
     * @param itemKey
     * @returns {string}
     */
    $scope.getPricingModel = function (itemKey) {
      return itemKey + PricingHelperService.getPricingSourceString($scope.pricingSource);
    };

    /**
     * Lisitner call back whiling updating zone.After that calling appendZoneName(asset); with asset.
     * @param $event
     */
    function onZoneDraw($event) {
      if ($scope.selectedAsset) {
        appendZoneName($scope.selectedAsset);
      }
    }

    /**
     * Updating asset meta data key with selected Asset property key
     * i.e. minimum asset properties
     * @param key
     */
    $scope.mapAssetMetaData = function (key) {
      if (labelSettingsMap[key]) {
        return labelSettingsMap[key];
      }
    };

    /**
     * Toggle Asset properties tab and tab data i.e Asset properties
     */
    $scope.toggleAssetTabs = function () {
      $scope.isAllTabLoaded = !$scope.isAllTabLoaded;
      if ($scope.isAllTabLoaded) {
        populateAssetTabData($scope.selectedAsset);
      }
      else {
        $scope.assetView = {
          tabs: '',
          tabContent: ''
        };
      }
    };


    $scope.isAllTabLoaded = false;

    //setting up listeners...
    $scope.$on(ViewEvent.ASSET_SELECT, onAssetSelect);
    $scope.$on(ViewEvent.ASSET_MAP, onAssetMap);
    $scope.$on(ViewEvent.ASSET_IMPORTED, onImportedAsset);
    $scope.$on(ViewEvent.ASSET_UPDATE, onAssetUpdate);
    $scope.$on(ViewEvent.ASSET_REPLOT, onAssetReplot);
    $scope.$on(ViewEvent.ASSET_ADD, onAssetAdd);
    $scope.$on(ViewEvent.LEVEL_SELECTED, onLevelSelect);
    $scope.$on(ViewEvent.LABEL_SETTING_UPDATE, onLabelSettingUpdate);
    $scope.$on(ViewEvent.ASSET_DELETED, onAssetDelete);
    $scope.$on(ViewEvent.OUTCOME_RESET, onOutcomeReset);
    $scope.$on(ViewEvent.OUTCOME_APPLY, onOutcomeApply);
    $scope.$on(ViewEvent.TRANSFER_UPDATE, onTransferUpdate);
    $scope.$on(ViewEvent.STATE_CHANGE, onStateChange);
    $scope.$on(ViewEvent.OUT_OF_SCOPE_SUCCESS, onAssetMove);
    $scope.$on(ViewEvent.ALL_ASSET_ACCESSORIES_DELETED, onAccessoriesDelete);
    $scope.$on(ViewEvent.ASSET_DRAG_END, onAssetDragEnd);
    $scope.$on(ViewEvent.ASSET_ZONE_UPDATED, onAssetZoneUpdated);
    $scope.$on(ViewEvent.ZONE_ASSETS_UPDATE, onZoneAssetsUpdated);
    $scope.$on(ViewEvent.REFRESH_COMPLETE, onRefreshComplete);
    $scope.$on('$destroy', onScopeDestroy);
    $scope.$on(ViewEvent.PRICING_SETTING_UPDATE, onPricingSettingUpdate);
    $scope.$on(ViewEvent.TABULAR_PRICING, getPricing);
    $scope.$on(ViewEvent.LEVEL_UPDATE, onLevelUpdate);
    $scope.$on(ViewEvent.ZONE_DRAWN, onZoneDraw);
  }

  var app = angular.module('app'),
      requires = [
        '$window',
        '$routeParams',
        '$rootScope',
        '$scope',
        '$log',
        '$filter',
        'Configuration',
        'ViewEvent',
        'ngCartosCore.constants.AssetProperties',
        'ngCartosUtils.services.SafeApplyService',
        'ngCartosCore.services.DeviceDataSetService',
        'HtmlFactory',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.services.IconService',
        'ngCartosCore.services.NotifierService',
        'ngCartosUtils.services.UtilityService',
        'SharedService',
        'CanvasAsset',
        'ngCartosCanvas.constants.AssetType',
        'DeviceColorMap',
        'StageHelperService',
        'ngCartosCore.services.AssetMapService',
        'RequestBuilder',
        'ngCartosCore.services.OutcomeService',
        'TransitionHelperService',
        'ngCartosCore.services.HpMasterDataMapService',
        'translateFilter',
        'ngCartosCore.services.ProjectSettingService',
        'ngCartosCore.services.AccessoriesDataMapService',
        'ngCartosCanvas.constants.OutcomeType',
        'ngCartosCore.services.DeviceAccessoriesService',
        '$timeout',
        'AssetPlacementHelper',
        'AmpvCalculationService',
        'ngCartosCore.constants.LevelType',
        'PricingHelperService',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.ZoneService',
        'ngCartosCore.services.LevelEntityMapService',
        'ngCartosCore.services.AssetLookUpService',
        'PageVolumeCalculationService',
        'AppSettingService',
        'ngCartosCore.services.PageVolumeService',
        'ngCartosCore.services.AssetMapHandler',
        AssetPropertyController
      ];
  app.controller('AssetPropertyController', requires);
}());
